# APftML2 Repository #

The APftML2 tool is a proof-of-concept implementation of an automated fairy tale
reader using an RDF/OWL document as information hub. It is able to detect
emotional speech on the fly and uses this information to enhance the voice
output.

APftML2 is licensed under LGPL, see `docs/LICENSE.TXT`.

***
## authors ##
name | role | main responsibility
---|---|---
Jana Ott | Python developer | emotion extraction
Tonio Süßdorf | Python developer |timeline extraction
Christian Willms | Python developer | dialogue extraction
Christian Eisenreich | Java developer | ontology integration, TTS
Thierry Declerck | adviser

## Acknowledgements ##

This project is build upon two previous works in the field of narrative text
processing. We thank the authors for their cooperation.

[APftML - Augmented Proppian fairy tale Markup Language](http://www.dfki.de/lt/publication_show.php?id=5050)

The described annotation scheme has been slightly extended and the manual
annotation of multiple fairy tales provided the gold standard.

[Ontology-Based Incremental Annotation of Characters in Folktales](http://www.dfki.de/lt/publication_show.php?id=6224)

This annotation technique is not fully integrated into this project and has to
be done manually. Please refer to `docs/creating the initial ontology.pdf` for a detailed explanation.

## Gold Standard ##

Five fairy tales have been manually annotated and validated against the APftML2 schema. These files are located in the `/soproworkspace/SoPro13Java/gold/` folder.

## Example Output ##

The computed output for one fairy tale text can be found in the `example output` folder.

## Prerequisites ##

The JAVA portion needs Java 1.7 or later installed. The Python portion depends
on Python 2.x and the following packages:

- PyYAML==3.11
- numpy==1.8.1
- Pattern==2.6

In addition, the following NLTK data packages are needed:

- Corpora/wordnet
- Models/maxent_treebank_pos_tagger
- Models/punkt

The easiest way to get these is to install NLTK and use the provided downloader.

    import nltk
    nltk.download()

Note that a specific NLTK version is embedded and will be used over the locally installed version.

## Eclipse ##

The main `.project` file is located in the `soproworkspace/SoPro13Java` folder.
Both the Java project and the embedded PyDev Project should be working out
of-the-box if everything is set up correctly.

## Javadoc ##

The javadoc of the Java portion is located at `docs/javadoc/`.
