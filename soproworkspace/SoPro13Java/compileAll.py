################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################
'''
Created on 17.05.2014

@author: christian
'''

import compileall

compileall.compile_dir('SoPro13Python/')
