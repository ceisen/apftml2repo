/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package utils;

import java.util.Comparator;

import tts.script.Dialogue;
import tts.script.Utterance;

/**
 * compares utterances by their reference value. This gives the proper order
 * when used in conjunction with {@link Dialogue}
 */
public class UtteranceComparator implements Comparator<Utterance> {

  /**
   * @param utt1
   *          first utterance
   * @param utt2
   *          second utterance
   * @return int value reflecting the lexical order
   */
  @Override
  public int compare(Utterance utt1, Utterance utt2) {

    return Integer.compare(utt1.getRef(), utt2.getRef());

  }

}