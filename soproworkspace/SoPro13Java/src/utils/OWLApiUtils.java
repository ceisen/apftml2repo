/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package utils;

import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

/**
 * common utility methods for the OWL API implementation
 */
public final class OWLApiUtils {

  /**
   * no instantiation
   */
  private OWLApiUtils() {

  }


  /**
   * filters a set of instances by the given fairy tale
   * 
   * @param instances
   *          a set of {@link OWLNamedIndividual}s
   * @param fairytaleID
   *          the fairy tale ID
   * @return a filtered subset
   */
  public static Set<OWLNamedIndividual> filterByTale(NodeSet<OWLNamedIndividual> instances, int fairytaleID) {

    Set<OWLNamedIndividual> filteredInstances = new HashSet<OWLNamedIndividual>();
    for (OWLNamedIndividual namedIndividual : instances.getFlattened()) {
      String name = namedIndividual.getIRI().getRemainder().get();
      if (name.contains("ft" + fairytaleID)) {
        filteredInstances.add(namedIndividual);
      }
    }
    return filteredInstances;
  }


  /**
   * computes the value of an object property
   * 
   * @param namedIndividual
   *          the instance properties are checked for
   * @param propertyName
   *          the property as String (e. g. "hasWife")
   * @param owlDataFactory
   *          a {@link OWLDataFactory}
   * @param prefixManager
   *          a {@link PrefixManager}
   * @param owlReasoner
   *          a {@link OWLReasoner}
   * @return a set of {@link OWLNamedIndividual}s containing the values or null if the property is not set
   */
  public static Set<OWLNamedIndividual> getObjectPropertyValue(OWLNamedIndividual namedIndividual,
      String propertyName, OWLDataFactory owlDataFactory, PrefixManager prefixManager, OWLReasoner owlReasoner) {

    OWLObjectProperty property =
      owlDataFactory.getOWLObjectProperty(propertyName, prefixManager);
    Set<OWLNamedIndividual> values =
      owlReasoner.getObjectPropertyValues(namedIndividual, property).getFlattened();

    return values;
  }
}
