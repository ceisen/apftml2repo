/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package utils;

import enums.SituationalMode;
import enums.SoundEffect;
import enums.SpeechMode;

/**
 * utility methods for simple object conversions
 * 
 */
public final class Converter {

  /**
   * no instances
   */
  private Converter() {

  };


  /**
   * simple converter from string to enum
   * 
   * @param enumAsString
   *          a string
   * @return the corresponding Enum or null
   */
  public static Enum<?> stringToEnum(String enumAsString) {

    switch (enumAsString) {

    // sitModes
      case "frightened":
        return SituationalMode.AFRAID;
      case "surprised":
        return SituationalMode.EXCITED;
      case "happy":
        return SituationalMode.HAPPY;
      case "weak":
        return SituationalMode.BORED;
      case "angry":
        return SituationalMode.ANGRY;
      case "sad":
        return SituationalMode.SAD;

        // speechModes
      case "answer":
        return SpeechMode.ANSWER;
      case "question":
        return SpeechMode.QUESTION;
      case "exclamation":
        return SpeechMode.EXCLAMATION;
      case "inform":
        return SpeechMode.INFORM;
      case "monologue":
        return SpeechMode.MONOLOGUE;
      default:
        return null;
    }
  }


  /**
   * converts a speech mode to an effect
   * 
   * @param speechMode
   *          a {@link SpeechMode}
   * @return a {@link SoundEffect}
   */
  public static SoundEffect modeToEffect(SpeechMode speechMode) {

    switch (speechMode) {
      case MONOLOGUE:
        return SoundEffect.INNER_MONOLOGUE;
      default:
        return null;
    }
  }


  /**
   * converts a string separated by spaces to camel case, starting with a lower letter (e. g. 'This is a test' will
   * result in 'thisIsATest'). A single word will be returned in lower case.
   * 
   * @param inputString
   *          a String to be converted to camel case
   * @return a String in camel case
   */
  public static String stringToCamelCase(String inputString) {

    if (!inputString.contains(" ")) {
      return inputString.toLowerCase();
    }

    StringBuilder outputBuilder = new StringBuilder();

    for (String nextWord : inputString.split(" ")) {
      String capitalizedNextWord = nextWord.substring(0, 1).toUpperCase() + nextWord.substring(1);
      outputBuilder.append(capitalizedNextWord);
    }

    String outputString = outputBuilder.toString();
    return outputString.substring(0, 1).toLowerCase() + outputString.substring(1);

  }
}
