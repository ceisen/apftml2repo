/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package utils;

import java.util.Comparator;

import tts.script.Dialogue;
import tts.script.Script;

/**
 * compares dialogues by their reference value. This gives the proper order when used in conjunction with
 * {@link Script}
 */
public class DialogueComparator implements Comparator<Dialogue> {

  /**
   * @param dia1
   *          first dialogue
   * @param dia2
   *          second dialogue
   * @return int value reflecting the lexical order
   */
  @Override
  public int compare(Dialogue dia1, Dialogue dia2) {

    return Integer.compare(dia1.getRef(), dia2.getRef());

  }

}