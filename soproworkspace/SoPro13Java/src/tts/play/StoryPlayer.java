/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.play;

import java.util.Map.Entry;

import javax.sound.sampled.AudioInputStream;
import javax.swing.event.EventListenerList;

import tts.play.event.AddNarratorEvent;
import tts.play.event.AddSenderEvent;
import tts.play.event.AudioEvent;
import tts.script.Dialogue;
import tts.script.Narrator;
import tts.script.Script;
import tts.script.Sender;
import tts.script.Utterance;
import tts.voice.VoiceMapper;
import tts.voice.VoiceMaryTTSImpl;
import enums.PresetVoice;

/**
 * Uses the MARY audio player to batch process a number of utterances in one go
 */
public class StoryPlayer {

  /**
   * A list of registered listeners
   */
  private final EventListenerList listeners = new EventListenerList();


  /**
   * plays back a fairy tale encoded as an ordered sequence of utterances
   * 
   * @param script
   *          a {@link Script} containing the dialogue to be played backed
   */
  public void play(Script script) {

    ConsoleAudioListener audioConsoleListener = new ConsoleAudioListener();
    this.listeners.add(AudioListener.class, audioConsoleListener);

    System.out.println("computing and playing audio...");

    VoiceMapper voiceMapper = new VoiceMapper();

    PresetVoice narratorVoice = voiceMapper.getNarratorVoice();
    VoiceMaryTTSImpl narrator = new VoiceMaryTTSImpl(narratorVoice);
    VoiceMaryTTSImpl[] voices = new VoiceMaryTTSImpl[script.getSenderCount()];

    for (AudioListener audioListener : this.listeners.getListeners(AudioListener.class)) {
      AddNarratorEvent addNarratorEvent = new AddNarratorEvent(this);
      audioListener.narratorAdded(addNarratorEvent);
    }

    AudioInputStream ais = null;

    for (Dialogue dialogue : script.getDialogues()) {
      for (Entry<Utterance, Sender> entry : dialogue.getDialogue().entrySet()) {
        if (entry.getValue() instanceof Narrator) {
          ais = narrator.generateSpeech(entry.getKey());

          AudioEvent audioEvent = new AudioEvent(this, ais, entry.getKey(), -1);
          for (AudioListener audioListener : this.listeners.getListeners(AudioListener.class)) {
            audioListener.speech(audioEvent);
          }
          continue;
        }
        int ref = entry.getValue().getRef();
        if (null == voices[ref]) {
          PresetVoice presetVoice = voiceMapper.voiceFromAttributes(entry.getValue().getAttributes());
          voices[ref] = new VoiceMaryTTSImpl(presetVoice);

          AddSenderEvent senderAddedEvent = new AddSenderEvent(this, entry.getValue(), ref, presetVoice);
          for (AudioListener audioListener : this.listeners.getListeners(AudioListener.class)) {
            audioListener.senderAdded(senderAddedEvent);
          }
        }
        ais = voices[ref].generateSpeech(entry.getKey());

        AudioEvent audioEvent = new AudioEvent(this, ais, entry.getKey(), ref);
        for (AudioListener audioListener : this.listeners.getListeners(AudioListener.class)) {
          audioListener.speech(audioEvent);
        }
      }
    }
  }
}
