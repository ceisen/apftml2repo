/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.play;

import java.util.EventListener;

import tts.play.event.AddNarratorEvent;
import tts.play.event.AddSenderEvent;
import tts.play.event.AudioEvent;

/**
 * An {@EventListener} that listens for speech output
 */
interface AudioListener extends EventListener {

  /**
   * @param audioEvent
   *          an {@link AudioEvent} fired by {@link StoryPlayer} when audio os played back
   */
  void speech(AudioEvent audioEvent);


  /**
   * @param addSenderEvent
   *          a {@link AddSenderEvent} fired by {@link StoryPlayer} when a new sender is introduced
   */
  void senderAdded(AddSenderEvent addSenderEvent);


  /**
   * @param addNarratorEvent
   *          a {@link AddSenderEvent} fired by {@link StoryPlayer} when a new sender is introduced
   */
  void narratorAdded(AddNarratorEvent addNarratorEvent);

}
