/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.play;

import javax.sound.sampled.AudioInputStream;

import tts.play.event.AddNarratorEvent;
import tts.play.event.AddSenderEvent;
import tts.play.event.AudioEvent;
import tts.script.Utterance;
import tts.script.UtteranceEmotional;
import marytts.util.data.audio.AudioPlayer;

/**
 * Implementation of {@link AudioListener} that prints the spoken utterance to the console
 */
public class ConsoleAudioListener implements AudioListener {

  /**
   * {@inheritDoc}
   */
  @Override
  public void speech(AudioEvent audioEvent) {

    Utterance utterance = audioEvent.getUtterance();

    if (!utterance.getSoundeffects().isEmpty()) {
      System.out.println("↓ Soundeffect added: " + utterance.getSoundeffects());
    }

    if (utterance instanceof UtteranceEmotional) {
      UtteranceEmotional utteranceEmotional = (UtteranceEmotional)utterance;
      System.out.printf("%-6s%s%s%n", "[" + audioEvent.getSenderID() + "]",
        "<" + utteranceEmotional.getEmotionAsString(true) + ">", utteranceEmotional.getContent());
    }
    else {
      System.out.printf("%-6s%s%n", "[" + audioEvent.getSenderID() + "]", utterance.getContent());
    }

    AudioInputStream audioInputstream = audioEvent.getAudioInputStream();
    new AudioPlayer(audioInputstream).run();

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void senderAdded(AddSenderEvent senderAddedEvent) {

    System.out.println("---------------------");
    System.out.println("sender added" + String.format("%n") + "\tID: " + senderAddedEvent.getSenderID());
    System.out.println("\tAttributes: " + senderAddedEvent.getSender().getAttributes());
    System.out.println("\tVoice: " + senderAddedEvent.getPresetVoice());
    System.out.println("---------------------");

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void narratorAdded(AddNarratorEvent addNarratorEvent) {

    System.out.println("---------------------");
    System.out.println("narrator added" + String.format("%n") + "\tID: " + "-1");
    System.out.println("---------------------");

  }

}
