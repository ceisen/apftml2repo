/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.play.event;

import java.util.EventObject;

import tts.play.StoryPlayer;
import tts.script.Sender;
import enums.PresetVoice;

/**
 * A speech event fired by the {@link StoryPlayer}
 */
public class AddSenderEvent extends EventObject {

  /**
   * the {@link Sender} that is introduced
   */
  private final Sender sender;

  /**
   * the unique sender ID
   */
  private int senderID;

  /**
   * the chosen {@link PresetVoice}
   */
  private PresetVoice presetVoice;


  /**
   * Creates a new instance of {@link AddSenderEvent}
   * 
   * @param source
   *          The object firing the event
   * @param sender
   *          a {@link Sender}
   * @param senderID
   *          unique sender id
   * @param presetVoice
   *          a {@link PresetVoice}
   */
  public AddSenderEvent(Object source, Sender sender, int senderID, PresetVoice presetVoice) {

    super(source);
    this.sender = sender;
    this.senderID = senderID;
    this.presetVoice = presetVoice;

  }


  /**
   * @return the sender
   */
  public Sender getSender() {

    return this.sender;
  }


  /**
   * @return the senderID
   */
  public int getSenderID() {

    return this.senderID;
  }


  /**
   * @return the presetVoice
   */
  public PresetVoice getPresetVoice() {

    return this.presetVoice;
  }

}
