/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.play.event;

import java.util.EventObject;

import tts.play.StoryPlayer;

/**
 * Fired by {@link StoryPlayer} when the narrator is added
 */
public class AddNarratorEvent extends EventObject {

  /**
   * Creates a new instance of {@link AddNarratorEvent}
   * 
   * @param source
   *          The object firing the event
   */
  public AddNarratorEvent(Object source) {

    super(source);

  }

}
