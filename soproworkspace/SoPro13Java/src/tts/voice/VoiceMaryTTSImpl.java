/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.voice;

import java.util.Locale;

import javax.sound.sampled.AudioInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import tts.script.Utterance;
import tts.script.UtteranceEmotional;
import enums.PresetVoice;

/**
 * MaryTTS based implementation of {@link Voice}
 * 
 * @see <a href="http://mary.dfki.de/">MaryTTS website</a>
 */
public final class VoiceMaryTTSImpl implements Voice {

  /**
   * parameters that modify the base voice (TractScale, F0Scale, F0Add). These are automatically applied to each
   * generated speech output
   */
  private String defaultVoiceModifier;

  /**
   * a remote Mary interface
   */
  private MaryInterface marytts;


  /**
   * Create a new instance of {@link VoiceMaryTTSImpl} from a predefined template
   * 
   * @param voice
   *          one of the {@link PresetVoice}s
   */
  public VoiceMaryTTSImpl(PresetVoice voice) {

    try {
      this.marytts = new LocalMaryInterface();
    } catch (MaryConfigurationException e) {
      e.printStackTrace();
    }

    switch (voice) {
      case EN_STORYTELLER:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-prudence-hsmm");
        this.defaultVoiceModifier = ("F0Scale(f0Scale:1.2)+F0Add(f0Add:-25)");
        break;
      case EN_TEENAGE_FEMALE:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-poppy-hsmm");
        break;
      case EN_LITTLE_GIRL:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-poppy-hsmm");
        this.defaultVoiceModifier =
          ("TractScaler(amount:1.3)+F0Scale(f0Scale:0.8)+"
            + "F0Add(f0Add:50)");
        break;
      case EN_EVIL_WITCH:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-poppy-hsmm");
        this.defaultVoiceModifier =
          ("TractScaler(amount:0.9)+F0Scale(f0Scale:1.5)+"
            + "Whisper(amount:0.75)+Chorus(delay1:0;amp1:1.0;delay2:500;"
            + "amp2:-0.7;delay3:1000;amp3:4.0)");
        break;
      case EN_FROGLIKE:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("cmu-bdl-hsmm");
        this.defaultVoiceModifier = ("F0Scale(f0Scale:1.5)+F0Add(f0Add:-25.0)");
        break;
      case EN_TREELIKE:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-spike-hsmm");
        this.defaultVoiceModifier =
          ("TractScaler(amount:0.83)+Whisper(amount:0.50)");
        break;
      case EN_ADULT_MALE_A:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-spike-hsmm");
        this.defaultVoiceModifier = ("F0Scale(f0Scale:1.3)+F0Add(f0Add:-10.0)");
        break;
      case EN_ADULT_MALE_B:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-obadiah-hsmm");
        this.defaultVoiceModifier = ("F0Scale(f0Scale:1.5)");
        break;
      case EN_ADULT_MALE_C:
        this.marytts.setLocale(Locale.US);
        this.marytts.setVoice("cmu-rms-hsmm");
        this.defaultVoiceModifier = ("F0Scale(f0Scale:1.5)");
        break;
      case EN_GNOMELIKE:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-spike-hsmm");
        this.defaultVoiceModifier =
          ("TractScaler(amount:1.6)+F0Scale(f0Scale:1.6)+ F0Add(f0Add:250.0)");
        break;
      case EN_DEVIOUS:
        this.marytts.setLocale(Locale.UK);
        this.marytts.setVoice("dfki-spike-hsmm");
        this.defaultVoiceModifier =
          ("Volume(amount:2.0)+TractScaler(amount:1.5)+F0Scale(f0Scale:1.3)+"
            + "F0Add(f0Add:-150.0)");
        break;
      default:
        break;
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public AudioInputStream generateSpeech(Utterance utterance) {

    // addition soundeffect specified?
    if (!utterance.getSoundeffects().isEmpty()) {
      try {
        this.marytts.setAudioEffects(this.defaultVoiceModifier.concat(utterance
          .getSoundeffects()));
      } catch (NullPointerException e) {
        this.marytts.setAudioEffects(utterance.getSoundeffects());
      }
    }
    else {
      this.marytts.setAudioEffects(this.defaultVoiceModifier);
    }

    // emotion specified?
    if (utterance instanceof UtteranceEmotional) {

      // create a minimal EmotionML document
      DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = null;
      Document doc = null;
      try {
        docBuilder = docFactory.newDocumentBuilder();
      } catch (ParserConfigurationException e) {
        e.printStackTrace();
      }
      if (docBuilder != null) {
        doc = docBuilder.newDocument();
        // header
        Element rootElement =
          doc.createElementNS("http://www.w3.org/2009/10/emotionml", "emotionml");
        rootElement.setAttribute("category-set",
          "http://www.w3.org/TR/emotion-voc/xml#everyday-categories");
        rootElement.setAttribute("version", "1.0");
        doc.appendChild(rootElement);

        // create the emotion Element
        Element emo =
          doc.createElementNS("http://www.w3.org/2009/10/emotionml", "emotion");
        Element cat =
          doc.createElementNS("http://www.w3.org/2009/10/emotionml", "category");
        cat.setAttribute("name",
          ((UtteranceEmotional)utterance).getEmotionAsString(false));
        emo.appendChild(cat);

        // create text
        Text tnode = doc.createTextNode(optimize(utterance.getContent()));
        emo.appendChild(tnode);
        rootElement.appendChild(emo);

        // pass the document to MaryTTS
        this.marytts.setInputType("EMOTIONML");
        try {
          return this.marytts.generateAudio(doc);
        } catch (SynthesisException e) {
          e.printStackTrace();
        }
      }
    }

    this.marytts.setInputType("TEXT");
    try {
      return (this.marytts.generateAudio(optimize(utterance.getContent())));
    } catch (SynthesisException e) {
      e.printStackTrace();
    }
    return null;
  }


  /**
   * Fixes some TTS issues by manipulating the utterance directly
   * 
   * @param content
   *          a string representing the utterance content
   * @return improved utterance content
   */
  private String optimize(String content) {

    content = StringUtils.replaceEach(content,
      new String[] { " 's", "do n't", "don't", "didn't", "lived", "ng.", "miller's" },
      new String[] { "s", "dont", "dont", "didnt", "lifd", "nng.", "millers" });
    return content;
  }
}
