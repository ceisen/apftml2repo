/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.voice;

import javax.sound.sampled.AudioInputStream;

import tts.script.Utterance;

/**
 * A wrapper for the TTS system of choice
 */
public interface Voice {

  /**
   * Creates a speech output of the given string using the TTS system.
   * 
   * @param utterance
   *          an {@link Utterance}
   * @return an AudioStream that can be played back
   */
  AudioInputStream generateSpeech(Utterance utterance);

}
