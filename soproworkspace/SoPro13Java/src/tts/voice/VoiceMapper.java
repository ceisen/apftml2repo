/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.voice;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Set;

import enums.PresetVoice;

/**
 * tries to match a voice to given arbitrary properties
 */
public class VoiceMapper {

  /**
   * a few adult voiced. Proof of concept for random selection
   */
  private ArrayList<PresetVoice> adultMaleVoices;

  /**
   * the voice collection
   */
  private VoicePool voicePool;


  /**
   * no instances
   */
  public VoiceMapper() {

    // will be randomly selected as proof of concept
    this.adultMaleVoices = new ArrayList<>();
    this.adultMaleVoices.add(PresetVoice.EN_ADULT_MALE_A);
    this.adultMaleVoices.add(PresetVoice.EN_ADULT_MALE_B);
    this.adultMaleVoices.add(PresetVoice.EN_ADULT_MALE_C);
    this.voicePool = new VoicePool();
  };


  /**
   * matches attributes to a voice
   * 
   * @param attributes
   *          some attributes, e.g. "woman"
   * @return a fitting {@link VoiceMaryTTSImpl}
   */
  public PresetVoice voiceFromAttributes(ArrayList<String> attributes) {

    PresetVoice presetVoice = null;

    LinkedList<String> weightedCategories = new LinkedList<String>();
    weightedCategories.add("Human");
    weightedCategories.add("Animal");
    weightedCategories.add("Supernatural");
    weightedCategories.add("Boy");
    weightedCategories.add("Man");
    weightedCategories.add("Girl");
    weightedCategories.add("Woman");

    Set<String> uniqueVoices = this.voicePool.getUniqueVoices();
    for (String uniqueVoice : uniqueVoices) {
      if (attributes.contains(uniqueVoice)) {
        return this.voicePool.getSenderVoice(uniqueVoice);
      }
    }

    for (String category : weightedCategories) {
      if (attributes.contains(category)) {
        presetVoice = this.voicePool.getSenderVoice(category);
      }
    }

    if (presetVoice == null) {
      System.out.println("now voice could be matched, defaulting to narrator");
      return this.voicePool.getNarratorVoice();
    }

    return presetVoice;
  }


  /**
   * asks the pool for the narrator voice
   * 
   * @return {@link PresetVoice}
   */
  public PresetVoice getNarratorVoice() {

    return this.voicePool.getNarratorVoice();
  }
}
