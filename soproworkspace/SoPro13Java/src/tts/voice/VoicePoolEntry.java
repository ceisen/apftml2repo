/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.voice;

import java.util.ArrayList;
import java.util.BitSet;
import enums.PresetVoice;

/**
 * helper class linking a BitSet to several voices
 */
public class VoicePoolEntry {

  /**
   * used to keep count of used voices
   */
  private BitSet usedVoices = new BitSet();

  /**
   * voices sharing a property e.g. 'male'
   */
  private ArrayList<PresetVoice> voices;


  /**
   * Creates a new instance of {@link VoicePoolEntry}.
   * 
   * @param voices
   *          a list of {@link PresetVoice}s
   */
  public VoicePoolEntry(ArrayList<PresetVoice> voices) {

    this.voices = voices;

  }


  /**
   * @return the usedVoices
   */
  public BitSet getUsedVoices() {

    return this.usedVoices;
  }


  /**
   * @return the voices
   */
  public ArrayList<PresetVoice> getVoices() {

    return this.voices;
  }

}
