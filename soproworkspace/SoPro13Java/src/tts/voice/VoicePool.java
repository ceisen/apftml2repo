/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.voice;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import enums.PresetVoice;

/**
 * The idea behind the voice pool is to proceed from the most general destinction to the most specific cases. The
 * categories are taken from the ontology
 */
public class VoicePool {

  /**
   * a complex object holding all needed voice information. BitSets are used to keep track of already used voices.
   */
  private HashMap<ArrayList<String>, VoicePoolEntry> pool;

  /**
   * unique voice
   */
  private HashMap<String, PresetVoice> uniqueVoices;

  /**
   * to randomly a select a voice
   */
  private Random randomizer;


  /**
   * Creates a new instance of {@link VoicePool}.
   */
  public VoicePool() {

    this.pool = new HashMap<>();
    this.randomizer = new Random();

    ArrayList<PresetVoice> humanVoices = new ArrayList<PresetVoice>();
    humanVoices.add(PresetVoice.EN_ADULT_MALE_A);
    humanVoices.add(PresetVoice.EN_ADULT_MALE_B);
    humanVoices.add(PresetVoice.EN_ADULT_MALE_C);
    humanVoices.add(PresetVoice.EN_LITTLE_GIRL);
    humanVoices.add(PresetVoice.EN_TEENAGE_FEMALE);
    ArrayList<String> humanClasses = new ArrayList<>();
    humanClasses.add("Human");
    this.pool.put(humanClasses, new VoicePoolEntry(humanVoices));

    ArrayList<PresetVoice> nonHumanVoices = new ArrayList<PresetVoice>();
    nonHumanVoices.add(PresetVoice.EN_EVIL_WITCH);
    nonHumanVoices.add(PresetVoice.EN_FROGLIKE);
    nonHumanVoices.add(PresetVoice.EN_GNOMELIKE);
    nonHumanVoices.add(PresetVoice.EN_TREELIKE);
    ArrayList<String> nonHumanClasses = new ArrayList<>();
    nonHumanClasses.add("Animal");
    nonHumanClasses.add("Supernatural");
    this.pool.put(nonHumanClasses, new VoicePoolEntry(nonHumanVoices));

    ArrayList<PresetVoice> maleVoices = new ArrayList<PresetVoice>();
    maleVoices.add(PresetVoice.EN_ADULT_MALE_A);
    maleVoices.add(PresetVoice.EN_ADULT_MALE_B);
    maleVoices.add(PresetVoice.EN_ADULT_MALE_C);
    ArrayList<String> maleHumanClasses = new ArrayList<>();
    maleHumanClasses.add("Man");
    maleHumanClasses.add("Boy");
    this.pool.put(maleHumanClasses, new VoicePoolEntry(maleVoices));

    ArrayList<PresetVoice> femaleVoices = new ArrayList<PresetVoice>();
    femaleVoices.add(PresetVoice.EN_TEENAGE_FEMALE);
    ArrayList<String> femaleHumanClasses = new ArrayList<>();
    femaleHumanClasses.add("Girl");
    femaleHumanClasses.add("Woman");
    this.pool.put(femaleHumanClasses, new VoicePoolEntry(femaleVoices));

    this.uniqueVoices = new HashMap<>();
    this.uniqueVoices.put("Frog", PresetVoice.EN_FROGLIKE);
    this.uniqueVoices.put("Witch", PresetVoice.EN_EVIL_WITCH);
    this.uniqueVoices.put("Goblin", PresetVoice.EN_GNOMELIKE);
    this.uniqueVoices.put("Talking Tree", PresetVoice.EN_TREELIKE);
    this.uniqueVoices.put("Little Girl", PresetVoice.EN_LITTLE_GIRL);

  }


  /**
   * return the default narrator voice
   * 
   * @return a {@link PresetVoice}
   */
  public PresetVoice getNarratorVoice() {

    return PresetVoice.EN_STORYTELLER;

  }


  /**
   * Return a voice of the given category. Note that null warnings have been suppressed since all relevant objects
   * are pre-initialized
   * 
   * @param category
   *          a OWL class name used to retrieve a voice
   * @return a {@link PresetVoice}
   */
  @SuppressWarnings("null")
  public PresetVoice getSenderVoice(String category) {

    if (this.uniqueVoices.containsKey(category)) {
      return this.uniqueVoices.get(category);
    }

    BitSet usedVoices = null;
    ArrayList<PresetVoice> voices = null;
    PresetVoice presetVoice = null;

    for (Entry<ArrayList<String>, VoicePoolEntry> entry : this.pool.entrySet()) {
      if (entry.getKey().contains(category)) {
        usedVoices = entry.getValue().getUsedVoices();
        voices = entry.getValue().getVoices();
      }
    }

    if (usedVoices.cardinality() == voices.size()) {
      System.out.println("voice pool for [" + category + "] exhausted");
    }

    // randomly pick next free voice
    int randomInt = this.randomizer.nextInt(voices.size());
    while (usedVoices.get(randomInt)) {
      randomInt = this.randomizer.nextInt(voices.size());
    }
    presetVoice = voices.get(randomInt);
    usedVoices.set(randomInt);

    return presetVoice;
  }


  /**
   * @return the uniqueVoices
   */
  public Set<String> getUniqueVoices() {

    return this.uniqueVoices.keySet();
  }

}
