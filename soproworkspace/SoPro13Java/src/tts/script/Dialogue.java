/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.script;

import java.util.ArrayList;
import java.util.TreeMap;

import utils.UtteranceComparator;

/**
 * {@link Dialogue} represents a dialigue as ordered {@link Utterance} sequence
 */
public class Dialogue {

  /**
   * unique reference
   */
  private int ref;

  /**
   * a list of {@link Sender}s partaking in the dialogue. Listeners are omitted since they are not relevant for
   * TTS.
   */
  private ArrayList<Sender> sendingParticipants;

  /**
   * A mapping from utterance to character sorted according to an {@link UtteranceComparator}
   */
  private TreeMap<Utterance, Sender> dialogue;


  /**
   * Creates a new instance of {@link Dialogue}.
   * 
   * @param ref
   *          a unique reference
   * 
   */
  public Dialogue(int ref) {

    this.ref = ref;
    this.sendingParticipants = new ArrayList<>();
    this.dialogue = new TreeMap<>(new UtteranceComparator());
  }


  /**
   * @return the sending participants
   */
  public ArrayList<Sender> getParticipants() {

    return this.sendingParticipants;
  }


  /**
   * @return the dialogue
   */
  public TreeMap<Utterance, Sender> getDialogue() {

    return this.dialogue;
  }


  /**
   * @return the ref
   */
  public int getRef() {

    return this.ref;
  }


  /**
   * @see java.lang.Object#toString()
   * @return a string representation
   */
  @Override
  public String toString() {

    return "<dia" + this.ref + this.sendingParticipants + this.dialogue.toString() + "/>"
      + String.format("%n") + "=============" + String.format("%n");

  }
}