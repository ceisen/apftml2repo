/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.script;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Comparator;

import utils.DialogueComparator;

/**
 * An {@link Script} is a container for {@link Dialogue} objects and represents the complete fairytale
 */
public class Script {

  /**
   * a list of {@link Dialogue}s
   */
  private ArrayList<Dialogue> dialogues;

  /**
   * indicates whether a character with the corresponding ref sends an utterance
   */
  private BitSet sendingCharacters;

  /**
   * used to sort the dialogues
   */
  private Comparator<Dialogue> dialogueComperator;


  /**
   * creates a new Instance of {@link Script}
   */
  public Script() {

    this.dialogues = new ArrayList<>();
    this.sendingCharacters = new BitSet();
    this.dialogueComperator = new DialogueComparator();
  }


  /**
   * @return the dialogues
   */
  public ArrayList<Dialogue> getDialogues() {

    return this.dialogues;
  }


  /**
   * adds a dialogue to the script
   * 
   * @param dialogue
   *          an {@link Dialogue} to add to the script
   */
  public void addDialogue(Dialogue dialogue) {

    for (Sender taleCharacter : dialogue.getParticipants()) {
      if (taleCharacter instanceof Narrator) {
        this.sendingCharacters.set(1000);
        continue;
      }
      this.sendingCharacters.set(taleCharacter.getRef());
    }

    this.dialogues.add(dialogue);
    java.util.Collections.sort(this.dialogues, this.dialogueComperator);
  }


  /**
   * @return the sender count
   */
  public int getSenderCount() {

    return this.sendingCharacters.cardinality();
  }
}