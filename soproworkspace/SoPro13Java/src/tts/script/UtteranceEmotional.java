/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.script;

import org.apache.commons.lang.StringUtils;

import enums.SituationalMode;
import enums.SpeechMode;

/**
 * An {@link UtteranceEmotional} represents an utterance with an additional {@link SituationalMode}
 */
public class UtteranceEmotional extends Utterance {

  /**
   * situational mode
   */
  private SituationalMode situationalMode;


  /**
   * Create a new instance of {@link UtteranceEmotional}
   * 
   * @param ref
   *          a unique reference e.g. utt23
   * @param content
   *          the text content of the utterance
   * @param speechMode
   *          a {@link SpeechMode}
   * @param situationalMode
   *          an {@link SituationalMode}
   */
  public UtteranceEmotional(int ref, String content, SpeechMode speechMode,
      SituationalMode situationalMode) {

    super(ref, content, speechMode);
    this.situationalMode = situationalMode;
  }


  /**
   * returns a string representation of the emotion
   * 
   * @param printMode
   *          <tt>true</tt> for printing, <tt>false</tt> for processing
   * @return a string representation of the emotion
   */
  public String getEmotionAsString(boolean printMode) {

    String emotionAsString = this.situationalMode.toString().toLowerCase();
    
    if (printMode) {
      emotionAsString = StringUtils.replaceEach(emotionAsString,
        new String[] { "bored", "excited" },
        new String[] { "weak", "surprised" });
    }
    return emotionAsString;
  }


  /**
   * @see java.lang.Object#toString()
   * @return a string representation
   */
  @Override
  public String toString() {

    return this.getRef() + " " + this.getContent() + "|sitMode:<"
      + this.situationalMode + ">|";

  }

}