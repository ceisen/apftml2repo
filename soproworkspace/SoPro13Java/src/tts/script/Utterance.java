/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.script;

import tts.voice.VoiceMaryTTSImpl;
import enums.SoundEffect;
import enums.SpeechMode;

/**
 * An {@link Utterance} represents an utterance
 */
public class Utterance {

  /**
   * unique reference
   */
  private int ref;

  /**
   * text content
   */
  private String content;

  /**
   * optional soundeffects
   */
  private String soundeffects;

  /**
   * a mode of speech e.g. 'answer'
   */
  private SpeechMode speechMode;


  /**
   * Create a new instance of {@link VoiceMaryTTSImpl}
   * 
   * @param ref
   *          a unique reference e.g. utt23
   * @param content
   *          the text content of the utterance
   * @param speechMode
   *          a {@link SpeechMode}
   */
  public Utterance(int ref, String content, SpeechMode speechMode) {

    this.ref = ref;
    this.content = content.toLowerCase();
    this.speechMode = speechMode;
    this.soundeffects = "";
  }


  /**
   * Adds an additional soundeffect to the Utterance
   * 
   * @param soundeffect
   *          a {@link SoundEffect}
   */
  public void addSoundEffect(SoundEffect soundeffect) {

    if (soundeffect != null) {
      switch (soundeffect) {
        case INNER_MONOLOGUE:
          this.soundeffects = "+Chorus(delay1:250;amp1:0.54;delay2:400;"
            + "amp2:-0.10;delay3:200;amp3:0.30)";
          break;
        default:
          break;
      }
    }
  }


  /**
   * @return the ref
   */
  public int getRef() {

    return this.ref;
  }


  /**
   * @return the content
   */
  public String getContent() {

    return this.content;
  }


  /**
   * @return the soundeffects
   */
  public String getSoundeffects() {

    return this.soundeffects;
  }


  /**
   * @see java.lang.Object#toString()
   * @return a string representation
   */
  @Override
  public String toString() {

    return "<utt" + this.ref + "|" + this.content + "|" + this.speechMode + "|" + this.soundeffects + "/>";
  }

}