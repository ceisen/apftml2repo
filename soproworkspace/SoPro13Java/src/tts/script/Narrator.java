/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.script;

/**
 * the narrator of a fairy tale
 * 
 */
public class Narrator extends Sender {

  /**
   * creates a new instance of {@link Narrator}
   * 
   * @param ref
   *          a unique reference
   */
  public Narrator(int ref) {

    super(ref);

  }
}
