/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package tts.script;

import java.util.ArrayList;

/**
 * {@link Sender} represents a Fairy tale character who is sender of at least one utterance. It holds all relevant
 * attributes that can be used for voice selection
 */
public class Sender {

  /**
   * a unique reference
   */
  private int ref;
  /**
   * the attributes of the character
   */
  private ArrayList<String> attributes;


  /**
   * Creates an {@link Sender}
   * 
   * @param ref
   *          a uniqe reference
   */
  public Sender(int ref) {

    this.ref = ref;
    this.attributes = new ArrayList<>();
  }


  /**
   * @return the attributes
   */
  public ArrayList<String> getAttributes() {

    return this.attributes;
  }


  /**
   * @return the ref
   */
  public int getRef() {

    return this.ref;
  }


  /**
   * @see java.lang.Object#toString()
   * @return a string representation
   */
  @Override
  public String toString() {

    return String.format(this.ref + "|" + this.attributes + "%n");
  }
}