/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.script;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import onto.populate.OntologyPopulatorOWLApiImpl;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

import tts.script.Dialogue;
import tts.script.Narrator;
import tts.script.Script;
import tts.script.Sender;
import tts.script.Utterance;
import tts.script.UtteranceEmotional;
import utils.Converter;
import utils.OWLApiUtils;
import enums.SituationalMode;
import enums.SpeechMode;

/**
 * OWL-API based implementation implementation of {@link ScriptExtractor}
 * 
 * @see <a href="http://owlapi.sourceforge.net/">OWL API website</a>
 */
public class ScriptExtractorOWLApiImpl implements ScriptExtractor {

  /** the ontology object */
  private OWLOntology owlOntology;

  /** manages all I/O operations */
  private OWLOntologyManager owlManager;

  /** creates entities and axioms */
  private OWLDataFactory owlDataFactory;

  /** holds a partial IRI to be prepended */
  private PrefixManager prefixManager;

  /**
   * used to query the ontology content
   */
  private OWLReasoner owlReasoner;


  /**
   * Creates a new instance of {@link OntologyPopulatorOWLApiImpl}.
   * 
   * @param input
   *          A {@link File} pointing to an OWL2 document
   * @param iriPrefix
   *          a string representing the default IRI prefix
   * @throws OWLOntologyCreationException
   *           when the ontology could not be loaded
   * 
   */
  public ScriptExtractorOWLApiImpl(File input, String iriPrefix)
      throws OWLOntologyCreationException {

    this.owlManager = OWLManager.createOWLOntologyManager();
    this.owlOntology = this.owlManager.loadOntologyFromOntologyDocument(input);
    this.owlDataFactory = this.owlManager.getOWLDataFactory();
    this.prefixManager = new DefaultPrefixManager(null, null, iriPrefix);

    OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
    this.owlReasoner = reasonerFactory.createReasoner(this.owlOntology);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public Script getScript(int fairytaleID) {

    Script script = new Script();
    HashMap<Integer, Sender> senders = new HashMap<>();

    // add the narrator
    Narrator narrator = new Narrator(-1);
    senders.put(-1, narrator);

    // get sending entities
    OWLClass senderClass = this.owlDataFactory.getOWLClass("Sender", this.prefixManager);
    NodeSet<OWLNamedIndividual> sendingInstances = this.owlReasoner.getInstances(senderClass, true);
    Set<OWLNamedIndividual> sendingInstancesFT = OWLApiUtils.filterByTale(sendingInstances, fairytaleID);

    for (OWLNamedIndividual namedIndividual : sendingInstancesFT) {

      // create sender and set attributes
      int id = extractID(namedIndividual);
      Sender sender = new Sender(extractID(namedIndividual));
      Collection<OWLClassExpression> types = EntitySearcher.getTypes(namedIndividual, this.owlOntology);

      Set<OWLClassExpression> enrichedTypes = new HashSet<>();

      // extract as many attributes as possible for voice mapping
      enrichedTypes.addAll(types);
      for (OWLClassExpression type : types) {
        Collection<OWLClassExpression> superTypes =
          EntitySearcher.getSuperClasses(type.asOWLClass(), this.owlOntology);
        enrichedTypes.addAll(superTypes);
      }
      for (OWLClassExpression type : enrichedTypes) {
        String typeName = type.toString().replaceAll(".*#(.*)>", "$1");
        sender.getAttributes().add(typeName);
      }
      senders.put(id, sender);
    }

    // get dialogue instances
    OWLClass dialogueClass = this.owlDataFactory.getOWLClass("DialogueEvent", this.prefixManager);
    NodeSet<OWLNamedIndividual> dialogueInstances = this.owlReasoner.getInstances(dialogueClass, true);
    Set<OWLNamedIndividual> dialogueInstancesFT = OWLApiUtils.filterByTale(dialogueInstances, fairytaleID);

    for (OWLNamedIndividual namedIndividual : dialogueInstancesFT) {
      Dialogue dialogue = new Dialogue(extractID(namedIndividual));

      // set the participants
      Set<OWLNamedIndividual> participants =
        OWLApiUtils.getObjectPropertyValue(namedIndividual, "hasParticipant", this.owlDataFactory,
          this.prefixManager, this.owlReasoner);
      for (OWLNamedIndividual participant : participants) {
        int id = extractID(participant);
        if (id == -1) {
          dialogue.getParticipants().add(narrator);
          continue;
        }
        if (id == -2) {
          // reader is skipped
          continue;
        }
        Sender participatingSenders = senders.get(extractID(participant));
        dialogue.getParticipants().add(participatingSenders);
      }

      // get the utterances of the dialogue
      Set<OWLNamedIndividual> utterances =
        OWLApiUtils.getObjectPropertyValue(namedIndividual, "hasUtterance", this.owlDataFactory,
          this.prefixManager, this.owlReasoner);

      String sitMode = null;

      for (OWLNamedIndividual utterance : utterances) {

        // get the sender
        Set<OWLNamedIndividual> sender =
          OWLApiUtils.getObjectPropertyValue(utterance, "hasSender", this.owlDataFactory, this.prefixManager,
            this.owlReasoner);
        int senderID = extractID(sender.iterator().next());
        Sender senderObj = senders.get(senderID);

        // get the speechMode
        Set<OWLNamedIndividual> utteranceSpeechMode =
          OWLApiUtils.getObjectPropertyValue(utterance, "hasSpeechMode", this.owlDataFactory, this.prefixManager,
            this.owlReasoner);
        OWLNamedIndividual speechMode = utteranceSpeechMode.iterator().next();
        String speechModeAsString = speechMode.toString().replaceAll(".*#(.*)>", "$1");
        SpeechMode speechModeEnum = (SpeechMode)Converter.stringToEnum(speechModeAsString);

        // get the situationalMode
        Set<OWLNamedIndividual> utteranceSitMode =
          OWLApiUtils.getObjectPropertyValue(utterance, "hasSituationalMode", this.owlDataFactory,
            this.prefixManager, this.owlReasoner);
        if (utteranceSitMode.size() > 0) {
          OWLNamedIndividual situationalMode = utteranceSitMode.iterator().next();
          sitMode = situationalMode.toString().replaceAll(".*#(.*)>", "$1");
        }

        // get the content
        OWLDataProperty hasStringContent =
          this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);
        Set<OWLLiteral> contentValues = this.owlReasoner.getDataPropertyValues(utterance, hasStringContent);
        String content = (contentValues.iterator().next()).getLiteral();

        // create the utterance
        if (sitMode != null) {
          SituationalMode situationalModeEnum = (SituationalMode)Converter.stringToEnum(sitMode);
          UtteranceEmotional utteranceEmotional =
            new UtteranceEmotional(extractID(utterance), content, speechModeEnum, situationalModeEnum);
          utteranceEmotional.addSoundEffect(Converter.modeToEffect(speechModeEnum));
          dialogue.getDialogue().put(utteranceEmotional, senderObj);
          sitMode = null;
          speechModeEnum = null;
          continue;
        }
        Utterance utteranceObj = new Utterance(extractID(utterance), content, speechModeEnum);
        utteranceObj.addSoundEffect(Converter.modeToEffect(speechModeEnum));
        dialogue.getDialogue().put(utteranceObj, senderObj);
        speechModeEnum = null;
      }
      script.addDialogue(dialogue);
    }
    return script;
  }


  /**
   * Converts a namedIndividual IRI to an ID
   * 
   * @param namedIndividual
   *          a {link OWLNamedIndividual}
   * @return an Integer id
   */
  private int extractID(OWLNamedIndividual namedIndividual) {

    String name = namedIndividual.getIRI().getRemainder().get();
    if (name.contains("narrator")) {
      return -1;
    }
    if (name.contains("reader")) {
      return -2;
    }
    String[] substrings = name.split("_");
    int id = Integer.parseInt(substrings[1].replaceAll("\\D+", ""));

    return id;
  }
}