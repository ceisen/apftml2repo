/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.script;

import tts.play.StoryPlayer;
import tts.script.Script;

/**
 * extracts dialogue information from an OWL ontology to a {@link Script} which can be passed to the {@link
 * StoryPlayer}
 */
public interface ScriptExtractor {

  /**
   * generates a TTS ready script from a given ontology. Only sending entities are included since receivers
   * including the reader have no immediate relevance for TTS
   * 
   * @param fairytaleID
   *          the fairytaleID
   * @return a {@link Script}
   */
  Script getScript(int fairytaleID);
}