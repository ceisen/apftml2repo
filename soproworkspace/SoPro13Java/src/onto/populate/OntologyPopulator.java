/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.populate;

import java.util.List;

import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 * The {@link OntologyPopulator} is designed to simplify the process of injecting dialogues into the ontology,
 * implicitly creating all necessary instances and properties
 */
public interface OntologyPopulator {

  /**
   * creates a fairytale instance with the given title
   * 
   * @param fairytaleID
   *          the fairy tale
   * @param title
   *          the title
   */
  void addTitle(int fairytaleID, String title);


  /**
   * Creates instances of Fulltext and FairyTale along with all needed properties
   * 
   * @param fairytaleID
   *          the fairy tale
   * @param fulltextContent
   *          the text content
   */
  void addFulltext(int fairytaleID, String fulltextContent);


  /**
   * looks up the fairy tale instance, gets the linked Fulltext instance and ads the given spans to its hasPart
   * property
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param spans
   *          a list of spans
   */
  void addSpans(int fairytaleID, List<String> spans);


  /**
   * adds an time instant (a specific point in time)
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param timeInstantID
   *          the id of the time instant
   */
  void addTimeInstant(int fairytaleID, int timeInstantID);


  /**
   * adds a plot event
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param plotEventID
   *          the ID of the event
   * @param sourceTimeID
   *          the current time instant ID
   * @param targetTimeID
   *          the time instant ID the relation points to
   * @param timeRelationType
   *          the type of the relation
   * @param eventContent
   *          the string content of the event
   * 
   */
  void addPlotEvent(int fairytaleID, int plotEventID, int sourceTimeID, int targetTimeID,
      String timeRelationType, String eventContent);


  /**
   * Creates an instance of Event > Dialogue along with all needes properties
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param dialogueID
   *          the id of the the dialogue
   * @param timeInstantStartID
   *          the id of the dialogue start time
   * @param timeInstantEndID
   *          the id of the dialogue end time
   */
  void addDialogueEvent(int fairytaleID, int dialogueID, int timeInstantStartID, int timeInstantEndID);


  /**
   * Adds the text, from beginning to end including the context
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param dialogueID
   *          the dialogue the participants are added to
   * @param stringContent
   *          the text content of the span
   */
  void setDialogueContent(int fairytaleID, int dialogueID, String stringContent);


  /**
   * Adds a participant to a dialogue. A 'hasParticipant' property will be added to the dialogue instance
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param dialogueID
   *          the dialogue the participants are added to
   * @param participant
   *          an expression referring to the participant
   */
  void addDialogueParticipant(int fairytaleID, int dialogueID, String participant);


  /**
   * Adds the given utterances to the dialogue. A new inferred class Sender will be generated, along with the
   * following new properties:
   * <p>
   * <ul>
   * <li>hasUtterance
   * <li>isUtteranceOf
   * <li>hasSender
   * <li>isSenderOf
   * <li>hasReceiver
   * <li>isReceiverOf
   * <li>hasSituationalMode
   * <li>isSituationalModeOf
   * <li>hasSpeechMode
   * <li>isSpeechModeOf
   * </ul>
   * </p>
   * 
   * @param fairytaleID
   *          the fairy tale instance
   * @param dialogueID
   *          the dialogue the utterances are part of
   * @param utteranceID
   *          the id of the utterance
   * @param situationalMode
   *          an apftml2 situationalMode
   * @param receiver
   *          the receiver of the Utterance
   * @param sender
   *          the receiver of the Utterance
   * @param timeInstantStartID
   *          the time point where the utterance starts
   * @param speechType
   *          an apftml2 speech Type
   * @param stringContent
   *          the Utterance content
   */
  void addDialogueUtterance(int fairytaleID, int dialogueID, int utteranceID, String situationalMode,
      String receiver, String sender, int timeInstantStartID, String speechType, String stringContent);


  /**
   * Returns the current axiom count
   * 
   * @return the axiom count
   */
  int getAxiomCount();


  /**
   * checks its internal consistency
   * 
   * @return true iff the ontology is consistent
   */
  boolean checkConsistency();


  /**
   * writes all changes to the OWL file
   * 
   * @throws OWLOntologyStorageException
   *           when the ontology can not be saved
   */
  void saveOntology()
      throws OWLOntologyStorageException;


  /**
   * creates a metadata instance containing meta information using information the ontology holds at this point
   * 
   * @param fairytaleID
   *          the fairy tale instance
   */
  void generateMetadata(int fairytaleID);

}