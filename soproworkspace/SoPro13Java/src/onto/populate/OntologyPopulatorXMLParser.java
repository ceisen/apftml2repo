/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.populate;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 * The {@link OntologyPopulatorXMLParser} parses a custom XML file created during the extraction phase and puts the result
 * into the ontology using a {@link OntologyPopulator}
 */
public class OntologyPopulatorXMLParser {

  /**
   * the Stax Parser
   */
  private XMLStreamReader parser;

  /**
   * a {@link InputStream}
   */
  private InputStream input;

  /**
   * a {@link OntologyPopulator} granting access to the OWL ontology file
   */
  private OntologyPopulator ontologyPopulator;

  /**
   * a fairy tale id
   */
  private int fairytaleID;


  /**
   * Creates a new instance of {@link OntologyPopulatorXMLParser}
   * 
   * @param inputPath
   *          A {@link Path} pointing to the xml file to be parsed
   * @param ontologyPopulator
   *          a {@link OntologyPopulator}
   * @param fairytaleID
   *          the fairy tale ID
   * @throws IOException
   *           if the file could not be read
   * @throws XMLStreamException
   *           if the file could not be read
   */
  public OntologyPopulatorXMLParser(Path inputPath, OntologyPopulator ontologyPopulator, int fairytaleID)
      throws IOException, XMLStreamException {

    this.input = Files.newInputStream(inputPath);
    XMLInputFactory factory = XMLInputFactory.newInstance();
    this.parser = factory.createXMLStreamReader(this.input);
    this.ontologyPopulator = ontologyPopulator;
    this.fairytaleID = fairytaleID;
  }


  /**
   * Parses the XML content, fills and saves the ontology
   * 
   * @return true iff the resulting ontology is larger and still consistent
   * @throws XMLStreamException
   *           STAX related
   * @throws IOException
   *           if the files could not be read
   * @throws OWLOntologyStorageException
   *           when the ontology could not be saved
   */
  public boolean parse()
      throws XMLStreamException, IOException, OWLOntologyStorageException {

    int axiomCountBefore = this.ontologyPopulator.getAxiomCount();

    StringBuilder fulltext = new StringBuilder();
    ArrayList<String> spans = new ArrayList<>();
    String title = null;
    int currentDialogue = 0;
    int currentTime = 0;
    String currentEventContent = null;
    int eventCounter = 0;

    while (this.parser.hasNext()) {
      switch (this.parser.getEventType()) {
        case XMLStreamConstants.START_ELEMENT:
          switch (this.parser.getLocalName()) {
            case "tale":
              title = this.parser.getAttributeValue(0);
              break;
            case "Span":
              String elementText = this.parser.getElementText();
              fulltext.append(elementText);
              spans.add(elementText);
              this.ontologyPopulator
                .setDialogueContent(this.fairytaleID, currentDialogue, elementText);
              break;
            case "time":
              currentTime = Integer.parseInt(this.parser.getAttributeValue(0));
              currentEventContent = this.parser.getAttributeValue(1);
              this.ontologyPopulator.addTimeInstant(this.fairytaleID, currentTime);
              break;
            case "Relation":
              this.ontologyPopulator.addPlotEvent(this.fairytaleID, eventCounter, currentTime,
                Integer.parseInt(this.parser.getAttributeValue(0)), this.parser.getAttributeValue(1),
                currentEventContent);
              break;
            case "dialogue":
              currentDialogue = Integer.parseInt(this.parser.getAttributeValue(0));
              this.ontologyPopulator.addDialogueEvent(this.fairytaleID, currentDialogue,
                Integer.parseInt(this.parser.getAttributeValue(2)),
                Integer.parseInt(this.parser.getAttributeValue(1)));
              break;
            case "participant":
              this.ontologyPopulator.addDialogueParticipant(this.fairytaleID, currentDialogue,
                this.parser.getAttributeValue(0));
              break;
            case "utterance":
              this.ontologyPopulator.addDialogueUtterance(
                this.fairytaleID,
                currentDialogue,
                Integer.parseInt(this.parser.getAttributeValue(0)),
                this.parser.getAttributeValue(1),
                this.parser.getAttributeValue(2),
                this.parser.getAttributeValue(3),
                Integer.parseInt(this.parser.getAttributeValue(4)),
                this.parser.getAttributeValue(5),
                this.parser.getElementText());
              break;
            default:
              break;
          }
          break;
        case XMLStreamConstants.END_ELEMENT:
          switch (this.parser.getLocalName()) {
            case "tale":
              this.ontologyPopulator.addTitle(this.fairytaleID, title);
              this.ontologyPopulator.addFulltext(this.fairytaleID, fulltext.toString());
              this.ontologyPopulator.addSpans(this.fairytaleID, spans);
              spans = new ArrayList<>();
              fulltext = new StringBuilder();
              break;
            case "Relations":
              eventCounter++;
              break;
            default:
              break;
          }
          break;
        default:
          break;
      }
      this.parser.next();
    }

    this.ontologyPopulator.generateMetadata(this.fairytaleID);
    this.parser.close();
    this.input.close();

    this.ontologyPopulator.saveOntology();

    if (this.ontologyPopulator.checkConsistency() && this.ontologyPopulator.getAxiomCount() > axiomCountBefore) {
      return true;
    }

    return false;
  }
}