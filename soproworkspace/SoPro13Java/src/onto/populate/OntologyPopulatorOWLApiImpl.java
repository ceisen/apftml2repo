/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.populate;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import utils.Converter;
import utils.OWLApiUtils;

/**
 * OWL-API based implementation of {@link OntologyPopulator}
 * 
 * @see <a href="http://owlapi.sourceforge.net/">OWL API website</a>
 */
public class OntologyPopulatorOWLApiImpl implements OntologyPopulator {

  /** the ontology object */
  private OWLOntology owlOntology;

  /** manages all I/O operations */
  private OWLOntologyManager owlManager;

  /** creates entities and axioms */
  private OWLDataFactory owlDataFactory;

  /** holds a partial IRI to be prepended */
  private PrefixManager prefixManager;

  /**
   * used to query the ontology content
   */
  private OWLReasoner owlReasoner;

  /**
   * target owl file
   */
  private File output;


  /**
   * Creates a new instance of {@link OntologyPopulatorOWLApiImpl}.
   * 
   * @param input
   *          A {@link File} pointing to an OWL2 document
   * @param output
   *          A {@link File} pointing to the resulting owl file
   * @param iriPrefix
   *          a string representing the default IRI prefix
   * @throws OWLOntologyCreationException
   *           when the ontology could not be loaded
   * 
   */
  public OntologyPopulatorOWLApiImpl(File input, File output, String iriPrefix)
      throws OWLOntologyCreationException {

    this.owlManager = OWLManager.createOWLOntologyManager();
    this.owlOntology = this.owlManager.loadOntologyFromOntologyDocument(input);
    this.owlDataFactory = this.owlManager.getOWLDataFactory();
    this.prefixManager = new DefaultPrefixManager(null, null, iriPrefix);
    this.output = output;

    OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
    this.owlReasoner = reasonerFactory.createReasoner(this.owlOntology);

  }


  /**
   * Creates a new instance of {@link OntologyPopulatorOWLApiImpl}. This is the version for in-place saving.
   * 
   * @param input
   *          A {@link File} pointing to an OWL2 document
   * @param iriPrefix
   *          a string representing the default IRI prefix
   * @throws OWLOntologyCreationException
   *           when the ontology could not be loaded
   * 
   */
  public OntologyPopulatorOWLApiImpl(File input, String iriPrefix)
      throws OWLOntologyCreationException {

    this.owlManager = OWLManager.createOWLOntologyManager();
    this.owlOntology = this.owlManager.loadOntologyFromOntologyDocument(input);
    this.owlDataFactory = this.owlManager.getOWLDataFactory();
    this.prefixManager = new DefaultPrefixManager(null, null, iriPrefix);
    this.output = input;

    OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
    this.owlReasoner = reasonerFactory.createReasoner(this.owlOntology);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addTitle(int fairytaleID, String title) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // get the correct Fairytale instance
    OWLIndividual fairyTaleInstance = getInstanceByPrefix("Fairytale", "ft" + fairytaleID);

    // set comment
    OWLAnnotationProperty commentProperty = this.owlDataFactory.getRDFSComment();
    OWLLiteral titleContent = this.owlDataFactory.getOWLLiteral(title, "");
    OWLAnnotation commentAnnotation = this.owlDataFactory.getOWLAnnotation(commentProperty, titleContent);
    axioms.add(this.owlDataFactory.getOWLAnnotationAssertionAxiom(fairyTaleInstance.asOWLNamedIndividual()
      .getIRI(), commentAnnotation));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addFulltext(int fairytaleID, String fulltextContent) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // create class Text>Fulltext
    OWLClass fullText =
      createClass("Text", "Fulltext", "The full text content of a fairy tale", "Volltext", "Fulltext");

    // create the instance
    OWLIndividual fulltextInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_fulltext", this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(fullText, fulltextInstance));

    // create the "hasText" object property and its inverse "isTextOf"
    OWLObjectProperty hasText = createObjectProperty("hasText", "text", "Fairytale", "Fulltext");
    setObjectPropertyFeatures(hasText, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    OWLObjectProperty isTextOf = createObjectProperty("isTextOf", "text", "Fulltext", "Fairytale");
    setObjectPropertyFeatures(isTextOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    axioms.add(this.owlDataFactory.getOWLInverseObjectPropertiesAxiom(hasText, isTextOf));

    // get the correct Fairytale instance
    OWLIndividual fairyTaleInstance = getInstanceByPrefix("Fairytale", "ft" + fairytaleID);

    // link the fulltext instance to the fairy tale instance
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasText, fairyTaleInstance,
      fulltextInstance));
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(isTextOf, fulltextInstance,
      fairyTaleInstance));

    // create the "hasStringContent" data property
    OWLDataProperty hasStringContent =
      this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);

    // add the content to the fulltext instance
    axioms.add(this.owlDataFactory.getOWLDataPropertyAssertionAxiom(hasStringContent, fulltextInstance,
      fulltextContent));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addSpans(int fairytaleID, List<String> spans) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // create Text>Span class
    OWLClass span = createClass("Text", "Span", "a span of text forming a coherent unit", "Abschnitt", "Span");

    // create the "hasSpan" and "isSpanOf" properties
    OWLObjectProperty hasSpan = createObjectProperty("hasSpan", "text", "Fulltext", "Span");
    OWLObjectProperty isSpanOf = createObjectProperty("isSpanOf", "text", "Span", "Fulltext");
    setObjectPropertyFeatures(isSpanOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    axioms.add(this.owlDataFactory.getOWLInverseObjectPropertiesAxiom(hasSpan, isSpanOf));

    // get the correct Fulltext Instance
    OWLIndividual fullTextInstance = getInstanceByPrefix("Fulltext", "ft" + fairytaleID);

    // create the span instances
    OWLDataProperty hasStringContent =
      this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);
    int index = 0;
    for (String spanContent : spans) {
      OWLIndividual spanInstance =
        this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_span" + index, this.prefixManager);
      axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(span, spanInstance));
      axioms.add(this.owlDataFactory.getOWLDataPropertyAssertionAxiom(hasStringContent, spanInstance,
        spanContent));
      // link the span to the fulltextInstance
      axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasSpan, fullTextInstance,
        spanInstance));
      axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(isSpanOf, spanInstance,
        fullTextInstance));
      index++;
    }

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void
      addTimeInstant(int fairytaleID, int timeInstantID) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // create Time>Instant class
    OWLClass instant = createClass("Time", "Instant", "a specific point in time", "Zeitpunkt", "Instant");

    // create the instance
    OWLIndividual instantInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_instant" + timeInstantID,
        this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(instant, instantInstance));

    // create the "time>hasTime" and "time>isTimeOf" properties
    OWLObjectProperty hasTime = createObjectProperty("hasTime", "time", "Fairytale", "Time");
    OWLObjectProperty isTimeOf = createObjectProperty("isTimeOf", "time", "Time", "Fairytale");
    setObjectPropertyFeatures(isTimeOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    axioms.add(this.owlDataFactory.getOWLInverseObjectPropertiesAxiom(hasTime, isTimeOf));

    if (timeInstantID - 1 > 0) {
      // create the "time>previousTime" property
      OWLObjectProperty previousTime = createObjectProperty("previousTime", "time", "Time", "Time");
      setObjectPropertyFeatures(previousTime, new int[] { 1, 0, 1, 0, 0, 0, 0 });
      // get the preceding timeInstant
      OWLIndividual previousTimeInstance =
        this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_instant" + (timeInstantID - 1),
          this.prefixManager);
      axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(previousTime, instantInstance,
        previousTimeInstance));
    }

    // link it to the fairytale
    OWLIndividual fairyTaleInstance = getInstanceByPrefix("Fairytale", "ft" + fairytaleID);
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasTime, fairyTaleInstance,
      instantInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(isTimeOf, instantInstance, fairyTaleInstance));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addPlotEvent(int fairytaleID, int plotEventID, int sourceTimeID, int targetTimeID,
      String timeRelationType, String eventContent) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // create the Event>PlotEvent class
    OWLClass plotEvent =
      createClass("Event", "PlotEvent",
        "an event that changes or advances the plot and can be temporaly related to another event",
        "Handlungsereignis",
        "PlotEvent");

    // create the instance
    OWLIndividual plotEventInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_plotEvent" + plotEventID,
        this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(plotEvent, plotEventInstance));

    // create the "hasStringContent" data property
    OWLDataProperty hasStringContent =
      this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);
    OWLDatatype stringDatatype = this.owlDataFactory.getOWLDatatype(OWL2Datatype.XSD_STRING.getIRI());
    axioms.add(this.owlDataFactory.getOWLDataPropertyRangeAxiom(hasStringContent, stringDatatype));
    axioms.add(this.owlDataFactory.getOWLFunctionalDataPropertyAxiom(hasStringContent));

    // set the content
    axioms.add(this.owlDataFactory.getOWLDataPropertyAssertionAxiom(hasStringContent, plotEventInstance,
      eventContent));

    if (targetTimeID == 0) {
      this.owlManager.addAxioms(this.owlOntology, axioms);
      return;
    }

    // create a object property named according to the given time relation
    String propertyName = Converter.stringToCamelCase(timeRelationType);
    OWLObjectProperty hasTimeRelation = createObjectProperty(propertyName, "time", "PlotEvent", "Instant");

    // create the time>startEvent and event>startsAt properties
    OWLObjectProperty startEvent = createObjectProperty("startEvent", "time", "Time", "Event");
    OWLObjectProperty startsAt = createObjectProperty("startsAt", "event", "Event", "Time");
    setObjectPropertyFeatures(startsAt, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    axioms.add(this.owlDataFactory.getOWLInverseObjectPropertiesAxiom(startEvent, startsAt));

    // link the plot event to the times
    OWLIndividual timeInstanceSource =
      getInstanceByPrefix("Instant", "ft" + fairytaleID + "_instant" + sourceTimeID);
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(startEvent, timeInstanceSource,
      plotEventInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(startsAt, plotEventInstance, timeInstanceSource));

    // get the target time instance of the time relation
    OWLIndividual targetTimeInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_instant" + targetTimeID,
        this.prefixManager);

    // get all plotEvents started by the target time instance
    Set<OWLNamedIndividual> relatedPlotEvents =
      OWLApiUtils.getObjectPropertyValue((OWLNamedIndividual)targetTimeInstance, "startEvent",
        this.owlDataFactory, this.prefixManager, this.owlReasoner);

    // link the relation to the events
    for (OWLNamedIndividual relatedPlotEvent : relatedPlotEvents) {
      // only plotEvents
      if (relatedPlotEvent.getIRI().toString().contains("plotEvent")) {
        axioms.add(this.owlDataFactory
          .getOWLObjectPropertyAssertionAxiom(hasTimeRelation, plotEventInstance, relatedPlotEvent));
      }
    }

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addDialogueEvent(int fairytaleID, int dialogueID, int timeInstantStartID, int timeInstantEndID) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // create Event>Dialogue class
    OWLClass dialogueEvent =
      createClass("Event", "DialogueEvent", "a dialogue", "Dialogereignis", "DialogueEvent");

    // create the instance
    OWLIndividual dialogueInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_dialogueEvent" + dialogueID,
        this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(dialogueEvent, dialogueInstance));

    // get the previously created start properties
    OWLObjectProperty startEvent = this.owlDataFactory.getOWLObjectProperty("startEvent", this.prefixManager);
    OWLObjectProperty startsAt = this.owlDataFactory.getOWLObjectProperty("startsAt", this.prefixManager);

    // link it to the times
    OWLIndividual timeInstanceStart =
      getInstanceByPrefix("Instant", "ft" + fairytaleID + "_instant" + timeInstantStartID);
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(startEvent, timeInstanceStart,
      dialogueInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(startsAt, dialogueInstance, timeInstanceStart));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void setDialogueContent(int fairytaleID, int dialogueID, String stringContent) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // get the dialogue instance
    OWLIndividual dialogueInstance =
      getInstanceByPrefix("DialogueEvent", "ft" + fairytaleID + "_dialogueEvent" + dialogueID);

    // set the content
    OWLDataProperty hasStringContent =
      this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLDataPropertyAssertionAxiom(hasStringContent, dialogueInstance,
      stringContent));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addDialogueParticipant(int fairytaleID, int dialogueID, String participant) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // resolve the reference
    OWLNamedIndividual participantInstance = resolveReference(fairytaleID, participant);
    if (participantInstance == null) {
      return;
    }

    // get the dialogue instance
    OWLIndividual dialogueInstance =
      getInstanceByPrefix("DialogueEvent", "ft" + fairytaleID + "_dialogueEvent" + dialogueID);

    // create the dialogue>hasParticipant and dialogue>isParticipantOf properties
    OWLObjectProperty hasParticipant =
      createObjectProperty("hasParticipant", "dialogue", "DialogueEvent", "Physical");
    OWLObjectProperty isParticipantOf =
      createObjectProperty("isParticipantOf", "dialogue", "Physical", "DialogueEvent");
    setObjectPropertyFeatures(isParticipantOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    axioms.add(this.owlDataFactory.getOWLInverseObjectPropertiesAxiom(hasParticipant, isParticipantOf));

    // add the participant to the dialogue
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasParticipant, dialogueInstance,
      participantInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(isParticipantOf, participantInstance, dialogueInstance));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void addDialogueUtterance(int fairytaleID, int dialogueID, int utteranceID, String situationalMode,
      String receiver, String sender, int timeInstantStartID, String speechMode, String stringContent) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // get the dialogue instance
    OWLIndividual dialogueInstance =
      getInstanceByPrefix("DialogueEvent", "ft" + fairytaleID + "_dialogueEvent" + dialogueID);

    // create the Dialogue>Utterance class
    OWLClass utterance =
      createClass("DialogueEvent", "Utterance", "a chain of spoken words", "Äußerung",
        "Utterance");

    // create the utterance instance
    OWLIndividual utteranceInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_utterance" + utteranceID,
        this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(utterance, utteranceInstance));

    // create the dialogue>hasUtteranceEvent and dialogue>isUtteranceEventOf properties
    OWLObjectProperty hasUtterance =
      createObjectProperty("hasUtterance", "dialogue", "DialogueEvent", "Utterance");
    OWLObjectProperty isUtteranceOf =
      createObjectProperty("isUtteranceOf", "dialogue", "Utterance", "DialogueEvent");
    setObjectPropertyFeatures(isUtteranceOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });

    // link the utterance to the dialogue
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasUtterance, dialogueInstance,
      utteranceInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(isUtteranceOf, utteranceInstance, dialogueInstance));

    // create Abstract>situationalMode and Abstract>SpeechMode classes
    OWLClass speechModeClass =
      createClass("Abstract", "SpeechMode", "a mode of speech", "Sprachmodus", "SpeechMode");
    OWLClass sitMode =
      createClass("Abstract", "SituationalMode", "a modifier of speech (often an emotional state)",
        "Situationsmodus", "SituationalMode");

    if (!situationalMode.equals("neutral")) {
      // add the situational mode instance
      OWLIndividual sitModeInstance =
        this.owlDataFactory.getOWLNamedIndividual(situationalMode, this.prefixManager);
      axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(sitMode, sitModeInstance));

      // create the hasSituationalMode and isSituationalModeOf properties
      OWLObjectProperty isSituationalModeOf =
        createObjectProperty("isSituationalModeOf", "dialogue", "SituationalMode", "Utterance");
      setObjectPropertyFeatures(isSituationalModeOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });
      OWLObjectProperty hasSituationalMode =
        createObjectProperty("hasSituationalMode", "dialogue", "Utterance", "SituationalMode");

      // link the sitMode to the utterance
      axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasSituationalMode, utteranceInstance,
        sitModeInstance));
      axioms.add(this.owlDataFactory
        .getOWLObjectPropertyAssertionAxiom(isSituationalModeOf, sitModeInstance, utteranceInstance));
    }

    // add the mode instance
    OWLIndividual speechModeInstance = this.owlDataFactory.getOWLNamedIndividual(speechMode, this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(speechModeClass, speechModeInstance));

    // create the hasSpeechMode and isSpeechModeOf properties
    OWLObjectProperty hasSpeechMode =
      createObjectProperty("hasSpeechMode", "dialogue", "Utterance", "SpeechMode");
    OWLObjectProperty isSpeechModeOf =
      createObjectProperty("isSpeechModeOf", "dialogue", "SpeechMode", "Utterance");
    setObjectPropertyFeatures(isSpeechModeOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });

    // link the type to the utterance
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasSpeechMode, utteranceInstance,
      speechModeInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(isSpeechModeOf, speechModeInstance, utteranceInstance));

    // create the hasSender and isSenderOf properties
    OWLObjectProperty hasSender =
      createObjectProperty("hasSender", "dialogue", "Utterance", "Physical");
    OWLObjectProperty isSenderOf =
      createObjectProperty("isSenderOf", "dialogue", "Physical", "Utterance");
    setObjectPropertyFeatures(isSpeechModeOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });

    // get the sender
    OWLNamedIndividual senderInstance = resolveReference(fairytaleID, sender);
    if (senderInstance == null) {
      return;
    }

    // link the sender to the utterance
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasSender, utteranceInstance,
      senderInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(isSenderOf, senderInstance, utteranceInstance));

    // create class Entity>Sender and link the sender to it
    OWLClass senderClass =
      createClass("Physical", "Sender", "entities that are sender of at least one utterance", "Sender", "Sender");
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(senderClass, senderInstance));

    // set the content
    OWLDataProperty hasStringContent =
      this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLDataPropertyAssertionAxiom(hasStringContent, utteranceInstance,
      stringContent));

    // create the hasReceiver and isReceiverOf properties
    OWLObjectProperty hasReceiver =
      createObjectProperty("hasReceiver", "dialogue", "Utterance", "Physical");
    OWLObjectProperty isReceiverOf =
      createObjectProperty("isReceiverOf", "dialogue", "Physical", "Utterance");
    setObjectPropertyFeatures(isReceiverOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });

    // get the receiver
    OWLNamedIndividual receiverInstance = resolveReference(fairytaleID, receiver);
    if (receiverInstance == null) {
      this.owlManager.addAxioms(this.owlOntology, axioms);
      return;
    }

    // link the receiver to the utterance
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasReceiver, utteranceInstance,
      receiverInstance));
    axioms.add(this.owlDataFactory
      .getOWLObjectPropertyAssertionAxiom(isReceiverOf, receiverInstance, utteranceInstance));

    // create class Entity>Receiver and link the receiver to it
    OWLClass receiverClass =
      createClass("Physical", "Receiver", "entities that are receiver of at least one utterance", "Empfänger",
        "Receiver");
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(receiverClass, receiverInstance));

    this.owlManager.addAxioms(this.owlOntology, axioms);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public boolean checkConsistency() {

    return this.owlReasoner.isConsistent();
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public int getAxiomCount() {

    return this.owlOntology.getAxiomCount();
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void generateMetadata(int fairytaleID) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // create the meta data class
    OWLClass metadata =
      createClass("", "Metadata", "additional information about a fairytale", "Metadaten", "Metadata");

    // create the meta data instance
    OWLIndividual metadataInstance =
      this.owlDataFactory.getOWLNamedIndividual("ft" + fairytaleID + "_metadata", this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLClassAssertionAxiom(metadata, metadataInstance));

    // create the metaData>hasMetadata and metaData>isMetadataOf properties
    OWLObjectProperty hasMetadata =
      createObjectProperty("hasMetadata", "meta", "Fairytale", "Metadata");
    setObjectPropertyFeatures(hasMetadata, new int[] { 1, 0, 0, 0, 0, 0, 0 });
    OWLObjectProperty isMetadataOf =
      createObjectProperty("isMetadataOf", "meta", "Metadata", "Fairytale");
    setObjectPropertyFeatures(isMetadataOf, new int[] { 1, 0, 0, 0, 0, 0, 0 });

    // link the meta data instance to the fairytale
    OWLIndividual fairyTaleInstance = getInstanceByPrefix("Fairytale", "ft" + fairytaleID);
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(hasMetadata, fairyTaleInstance,
      metadataInstance));
    axioms.add(this.owlDataFactory.getOWLObjectPropertyAssertionAxiom(isMetadataOf, metadataInstance,
      fairyTaleInstance));

    // wordcount
    OWLClass fulltextClass = this.owlDataFactory.getOWLClass("Fulltext", this.prefixManager);
    NodeSet<OWLNamedIndividual> fulltextInstances = this.owlReasoner.getInstances(fulltextClass, true);
    Set<OWLNamedIndividual> fulltextInstancesFT = OWLApiUtils.filterByTale(fulltextInstances, fairytaleID);
    OWLNamedIndividual fulltext = fulltextInstancesFT.iterator().next();
    OWLDataProperty hasStringContent =
      this.owlDataFactory.getOWLDataProperty("hasStringContent", this.prefixManager);
    Set<OWLLiteral> contentValues = this.owlReasoner.getDataPropertyValues(fulltext, hasStringContent);
    String content = (contentValues.iterator().next()).getLiteral();
    String[] splitted = content.split("\\s+");
    int wordCountValue = splitted.length;
    OWLDataProperty wordCount = this.owlDataFactory.getOWLDataProperty("words", this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLDataPropertyAssertionAxiom(wordCount, metadataInstance, wordCountValue));

    this.owlManager.addAxioms(this.owlOntology, axioms);

    // other counts
    String[] classes = new String[] { "Sender", "Receiver", "DialogueEvent", "PlotEvent", "Instant", "Span" };
    for (String oneClass : classes) {
      addInstanceCount(fairytaleID, oneClass, metadataInstance);
    }

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void saveOntology()
      throws OWLOntologyStorageException {

    this.owlManager.saveOntology(this.owlOntology, IRI.create(this.output.toURI()));
  }


  /**
   * gets an instance by class and prefix
   * 
   * @param className
   *          the class
   * @param prefix
   *          a unique prefix (e.g. "ch4")
   * @return an {@link OWLIndividual}
   */
  private OWLIndividual getInstanceByPrefix(String className, String prefix) {

    OWLClass containingClass = this.owlDataFactory.getOWLClass(className, this.prefixManager);
    NodeSet<OWLNamedIndividual> characterInstances = this.owlReasoner.getInstances(containingClass, true);
    for (OWLNamedIndividual namedIndividual : characterInstances.getFlattened()) {
      if (namedIndividual.getIRI().getRemainder().get().startsWith(prefix)) {
        return namedIndividual;
      }
    }
    return null;
  }


  /**
   * creates a new class
   * 
   * @param parent
   *          the name of the parent class or null to create a top level class
   * @param name
   *          the name of the new class
   * @param comment
   *          an english comment
   * @param labelDE
   *          german label
   * @param labelEN
   *          english label
   * @return an {link OWLClass}
   */
  private OWLClass createClass(String parent, String name, String comment, String labelDE, String labelEN) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
    OWLClass newClass = this.owlDataFactory.getOWLClass(name, this.prefixManager);

    // set comment
    OWLAnnotationProperty commentProperty = this.owlDataFactory.getRDFSComment();
    OWLLiteral commentContent = this.owlDataFactory.getOWLLiteral(comment, "");
    OWLAnnotation commentAnnotation = this.owlDataFactory.getOWLAnnotation(commentProperty, commentContent);
    axioms.add(this.owlDataFactory.getOWLAnnotationAssertionAxiom(newClass.getIRI(), commentAnnotation));

    // set labels
    OWLAnnotationProperty labelProperty = this.owlDataFactory.getRDFSLabel();
    OWLLiteral labelContentEN = this.owlDataFactory.getOWLLiteral(labelEN, "en");
    OWLAnnotation labelAnnotationEN = this.owlDataFactory.getOWLAnnotation(labelProperty, labelContentEN);
    axioms.add(this.owlDataFactory.getOWLAnnotationAssertionAxiom(newClass.getIRI(), labelAnnotationEN));

    OWLLiteral labelContentDE = this.owlDataFactory.getOWLLiteral(labelDE, "de");
    OWLAnnotation labelAnnotationDE = this.owlDataFactory.getOWLAnnotation(labelProperty, labelContentDE);
    axioms.add(this.owlDataFactory.getOWLAnnotationAssertionAxiom(newClass.getIRI(), labelAnnotationDE));

    // add the new class to the ontology
    if (!parent.isEmpty()) {
      OWLClass parentClass = this.owlDataFactory.getOWLClass(parent, this.prefixManager);
      axioms.add(this.owlDataFactory.getOWLSubClassOfAxiom(newClass, parentClass));
    }

    this.owlManager.addAxioms(this.owlOntology, axioms);
    return newClass;

  }


  /**
   * creates a new object property
   * 
   * @param name
   *          the name of the new property
   * @param parent
   *          the name of the parent property or null to create a top level property
   * @param domain
   *          the domain of the new property
   * @param range
   *          the range of the new property
   * @return an {@link OWLObjectProperty}
   */

  private OWLObjectProperty createObjectProperty(String name, String parent, String domain, String range) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    OWLObjectProperty newProperty = this.owlDataFactory.getOWLObjectProperty(name, this.prefixManager);
    OWLClass domainClass = this.owlDataFactory.getOWLClass(domain, this.prefixManager);
    OWLClass rangeClass = this.owlDataFactory.getOWLClass(range, this.prefixManager);
    axioms.add(this.owlDataFactory.getOWLObjectPropertyDomainAxiom(newProperty, domainClass));
    axioms.add(this.owlDataFactory.getOWLObjectPropertyRangeAxiom(newProperty, rangeClass));

    if (!parent.isEmpty()) {
      OWLObjectProperty parentProperty = this.owlDataFactory.getOWLObjectProperty(parent, this.prefixManager);
      axioms.add(this.owlDataFactory.getOWLSubObjectPropertyOfAxiom(newProperty, parentProperty));
    }

    this.owlManager.addAxioms(this.owlOntology, axioms);
    return newProperty;
  }


  /**
   * sets the features of an object property
   * 
   * @param property
   *          An {@link OWLObjectProperty} to set the features of
   * @param features
   *          an int Array used to set the features as follows: [0] functional [1] inverse functional [2]
   *          transitive [3] symmetric [4] asymmetric [5] reflexive [6] irreflexive. For example
   *          "asymmetric, reflexive" would be called with <code>new int[] {0, 0, 0, 0, 1, 1, 0}</code>
   */

  private void setObjectPropertyFeatures(OWLObjectProperty property, int[] features) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    if (features[0] == 1) {
      axioms.add(this.owlDataFactory.getOWLFunctionalObjectPropertyAxiom(property));
    }
    if (features[1] == 1) {
      axioms.add(this.owlDataFactory.getOWLInverseFunctionalObjectPropertyAxiom(property));
    }
    if (features[2] == 1) {
      axioms.add(this.owlDataFactory.getOWLTransitiveObjectPropertyAxiom(property));
    }
    if (features[3] == 1) {
      axioms.add(this.owlDataFactory.getOWLSymmetricObjectPropertyAxiom(property));
    }
    if (features[4] == 1) {
      axioms.add(this.owlDataFactory.getOWLAsymmetricObjectPropertyAxiom(property));
    }
    if (features[5] == 1) {
      axioms.add(this.owlDataFactory.getOWLReflexiveObjectPropertyAxiom(property));
    }
    if (features[6] == 1) {
      axioms.add(this.owlDataFactory.getOWLIrreflexiveObjectPropertyAxiom(property));
    }

    this.owlManager.addAxioms(this.owlOntology, axioms);
  }


  /**
   * resolves a reference to the referred character
   * 
   * @param fairytaleID
   *          the id of the fairytale
   * @param referenceString
   *          string reference to be resolved
   * @return the character as OWLNamedIndividual
   */
  private OWLNamedIndividual resolveReference(int fairytaleID, String referenceString) {

    if (referenceString.equals("narrator") || referenceString.equals("reader")) {
      return this.owlDataFactory.getOWLNamedIndividual(referenceString, this.prefixManager);
    }

    if (referenceString.contains("_")) {
      String[] substrings = referenceString.split("_");
      referenceString = (substrings[0] + "_" + fairytaleID + "_" + substrings[1]).replace(" ", "_");
    }
    else {
      referenceString = referenceString + "_" + fairytaleID;
    }

    // get the instance of the referring expression and use the reasoner to get the correct character
    OWLNamedIndividual reference =
      this.owlDataFactory.getOWLNamedIndividual(referenceString.replace(" ", "_"), this.prefixManager);
    Node<OWLNamedIndividual> sameIndividuals = this.owlReasoner.getSameIndividuals(reference);

    OWLNamedIndividual characterInstance = null;
    for (OWLNamedIndividual oWLNamedIndividual : sameIndividuals) {
      // get the name
      if (oWLNamedIndividual.getIRI().getRemainder().get().startsWith("ft" + fairytaleID + "_ch")) {
        characterInstance = oWLNamedIndividual;
      }
    }
    return characterInstance;
  }


  /**
   * ads the number of instances of a class to a metadata instance
   * 
   * @param fairytaleID
   *          the fairy tale id
   * @param className
   *          the name of the class
   * @param metadataInstance
   *          an {@link OWLIndividual}
   */
  private void addInstanceCount(int fairytaleID, String className, OWLIndividual metadataInstance) {

    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    String propertyName = className.toLowerCase() + "s";
    propertyName = propertyName.replace("event", "Event");

    OWLClass owlClass = this.owlDataFactory.getOWLClass(className, this.prefixManager);
    NodeSet<OWLNamedIndividual> classInstances = this.owlReasoner.getInstances(owlClass, true);
    Set<OWLNamedIndividual> classInstancesFT = OWLApiUtils.filterByTale(classInstances, fairytaleID);
    OWLDataProperty speakerCount = this.owlDataFactory.getOWLDataProperty(propertyName, this.prefixManager);
    axioms.add(this.owlDataFactory
      .getOWLDataPropertyAssertionAxiom(speakerCount, metadataInstance, classInstancesFT.size()));

    this.owlManager.addAxioms(this.owlOntology, axioms);
  }
}
