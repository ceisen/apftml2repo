/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.ref;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import onto.populate.OntologyPopulatorOWLApiImpl;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

/**
 * OWL-API based implementation of {@link OntologyRefLookup}
 * 
 * @see <a href="http://owlapi.sourceforge.net/">OWL API website</a>
 */
public class OntologyRefLookupOWLApiImpl implements OntologyRefLookup {

  /** the ontology object */
  private OWLOntology owlOntology;

  /** manages all I/O operations */
  private OWLOntologyManager owlManager;

  /** creates entities and axioms */
  private OWLDataFactory owlDataFactory;

  /** holds a partial IRI to be prepended */
  private PrefixManager prefixManager;

  /**
   * used to query the ontology content
   */
  private OWLReasoner owlReasoner;


  /**
   * Creates a new instance of {@link OntologyPopulatorOWLApiImpl}
   * 
   * @param input
   *          A {@link File} pointing to an OWL2 document
   * @param iriPrefix
   *          a string representing the default IRI prefix
   * @throws OWLOntologyCreationException
   *           when the ontology could not be loaded
   * 
   */
  public OntologyRefLookupOWLApiImpl(File input, String iriPrefix)
      throws OWLOntologyCreationException {

    this.owlManager = OWLManager.createOWLOntologyManager();
    this.owlOntology = this.owlManager.loadOntologyFromOntologyDocument(input);
    this.owlDataFactory = this.owlManager.getOWLDataFactory();
    this.prefixManager = new DefaultPrefixManager(null, null, iriPrefix);

    OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
    this.owlReasoner = reasonerFactory.createReasoner(this.owlOntology);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public HashMap<String, HashSet<String>> lookup(int fairytaleID) {

    HashMap<String, HashSet<String>> lookupResult = new HashMap<>();

    // get all Character instances
    OWLClass character = this.owlDataFactory.getOWLClass("Character", this.prefixManager);
    Set<OWLNamedIndividual> characterInstances = this.owlReasoner.getInstances(character, true).getFlattened();

    // for each of those, all referencing expressions (sameAs)
    for (OWLNamedIndividual namedIndividual : characterInstances) {
      // filter by fairy tale id
      if (namedIndividual.getIRI().getRemainder().get().startsWith("ft" + fairytaleID)) {
        String characterString = namedIndividual.getIRI().getRemainder().get().toString();
        HashSet<String> references = new HashSet<String>();
        lookupResult.put(characterString, references);
        Node<OWLNamedIndividual> sameIndividuals = this.owlReasoner.getSameIndividuals(namedIndividual);
        for (OWLNamedIndividual sameIndividual : sameIndividuals) {
          references.add(sameIndividual.getIRI().getRemainder().get());
        }
      }
    }
    return lookupResult;
  }
}