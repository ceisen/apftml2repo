/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.ref;

import java.util.HashMap;
import java.util.HashSet;

/**
 * looks up reference expressions in an ontology
 * 
 */
public interface OntologyRefLookup {

  /**
   * extracts the references from an ontology that was manually pre-filled with referring expressions
   * 
   * @param fairytaleID
   *          a fairy tale id
   * @return a {@link HashMap} mapping the character to its referring expressions
   */
  HashMap<String, HashSet<String>> lookup(int fairytaleID);

}
