/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package onto.ref;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import javanet.staxutils.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import onto.populate.OntologyPopulator;

/**
 * The {@link RefLookupXMLWriter} writes a minimal XML file by looking up references in the ontology using a
 * {@link OntologyRefLookup}
 */
public class RefLookupXMLWriter {

  /**
   * the Stax Writer
   */
  private XMLStreamWriter writer;

  /**
   * a {@link OntologyRefLookup} granting access to the OWL ontology file
   */
  private OntologyRefLookup ontologyRefLookup;

  /**
   * a fairy tale id
   */
  private int fairytaleID;

  /**
   * a {@link FileOutputStream}
   */
  private FileOutputStream output;


  /**
   * Creates a new instance of {@link RefLookupXMLWriter}
   * 
   * @param outputXML
   *          a {@link FileOutputStream}
   * @param ontologyRefLookup
   *          a {@link OntologyPopulator}
   * @param fairytaleID
   *          the fairy tale ID
   * @throws XMLStreamException
   *           if the file could not be read
   */
  public RefLookupXMLWriter(FileOutputStream outputXML, OntologyRefLookup ontologyRefLookup, int fairytaleID)
      throws XMLStreamException {

    this.output = outputXML;
    XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
    XMLStreamWriter streamWriter = outputFactory.createXMLStreamWriter(this.output);
    this.writer = new IndentingXMLStreamWriter(streamWriter);

    this.ontologyRefLookup = ontologyRefLookup;
    this.fairytaleID = fairytaleID;
  }


  /**
   * looks up references in an ontology and writes a minimal XML document
   * 
   * @return true iff the result could be written successfully
   * @throws XMLStreamException
   *           STAX related
   */
  public boolean write()
      throws XMLStreamException {

    HashMap<String, HashSet<String>> references = this.ontologyRefLookup.lookup(this.fairytaleID);
    if (references.isEmpty()) {
      System.exit(1);
    }

    this.writer.writeStartDocument();
    this.writer.writeStartElement("references");
    this.writer.writeStartElement("narrator");
    this.writer.writeEndElement();
    this.writer.writeStartElement("reader");
    this.writer.writeEndElement();

    for (Entry<String, HashSet<String>> entry : references.entrySet()) {
      String character = entry.getKey();
      if (character != null && !character.equals("narrator") && !character.equals("reader")) {
        HashSet<String> referenceList = entry.getValue();
        for (String reference : referenceList) {
          this.writer.writeStartElement(reference.replace(" ", "_"));
          this.writer.writeAttribute("ref", character);
          this.writer.writeEndElement();
        }
      }
    }
    this.writer.writeEndElement();
    this.writer.writeEndDocument();
    this.writer.close();

    return true;

  }
}