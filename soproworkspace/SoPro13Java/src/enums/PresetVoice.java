/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package enums;

import tts.voice.VoiceMaryTTSImpl;

/**
 * common fairy tale voices to be used in conjunction with the template
 * constructor in {@link VoiceMaryTTSImpl}
 */
public enum PresetVoice {
  /**
   * storyteller, en
   */
  EN_STORYTELLER,

  /**
   * male, adult, en-GB
   */
  EN_ADULT_MALE_A,

  /**
   * male, adult, en-GB, sounds a bit bored or weak
   */
  EN_ADULT_MALE_B,

  /**
   * male, adult, en-US, sounds a bit older
   */
  EN_ADULT_MALE_C,

  /**
   * female, teenager, en-GB
   */
  EN_TEENAGE_FEMALE,

  /**
   * little girl, en-GB
   */
  EN_LITTLE_GIRL,

  /**
   * evil witch, uses Chorus as default effect, en-GB
   */
  EN_EVIL_WITCH,

  /**
   * A very croaky voice, en-GB
   */
  EN_FROGLIKE,

  /**
   * A deep voice, en-GB
   */
  EN_TREELIKE,

  /**
   * high pitched croaky voice, en-GB. For gnomes or somesuch
   */
  EN_GNOMELIKE,

  /**
   * A whispery, devious voice.
   */
  EN_DEVIOUS

};
