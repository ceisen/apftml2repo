/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package enums;

/**
 * the "everyday" emotions as specified by the EmotionML standard by W3C
 * 
 * @see <a href=
 *      "http://www.w3.org/TR/emotion-voc/xml#everyday-categories">EmotionML
 *      specification/</a>
 * 
 */
public enum SituationalMode {
  /**
   * affectionate
   */
  AFFECTIONATE,

  /**
   * afraid
   */
  AFRAID,

  /**
   * amused
   */
  AMUSED,

  /**
   * angry
   */
  ANGRY,

  /**
   * bored
   */
  BORED,

  /**
   * confident
   */
  CONFIDENT,

  /**
   * content
   */
  CONTENT,

  /**
   * disappointed
   */
  DISAPPOINTED,

  /**
   * excited
   */
  EXCITED,

  /**
   * happy
   */
  HAPPY,

  /**
   * interested
   */
  INTERESTED,

  /**
   * loving
   */
  LOVING,

  /**
   * pleased
   */
  PLEASED,

  /**
   * relaxed
   */
  RELAXED,

  /**
   * sad
   */
  SAD,

  /**
   * satisfied
   */
  SATISFIED,

  /**
   * worried
   */
  WORRIED
}
