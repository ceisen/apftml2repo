/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package enums;

/**
 * additional soundeffects that can be added to an utterance
 */
public enum SoundEffect {
  /**
   * inner monologue, adds reverb
   */
  INNER_MONOLOGUE
}
