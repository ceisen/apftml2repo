/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package enums;

/**
 * the five most basic speech modes
 */
public enum SpeechMode {
  /**
   * inform
   */
  INFORM,

  /**
   * question
   */
  QUESTION,

  /**
   * question
   */
  ANSWER,

  /**
   * question
   */
  MONOLOGUE,

  /**
   * question
   */
  EXCLAMATION,

};
