/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package test;

import javax.sound.sampled.AudioInputStream;

import marytts.util.data.audio.AudioPlayer;

import org.junit.Test;

import tts.script.Utterance;
import tts.script.UtteranceEmotional;
import tts.voice.VoiceMaryTTSImpl;
import enums.PresetVoice;
import enums.SituationalMode;
import enums.SpeechMode;

/**
 * {@link TestCharacterVoice} is a test class for {@link VoiceMaryTTSImpl}
 */
public class TestCharacterVoice {

  @SuppressWarnings("javadoc")
  @Test
  public void test() {

    VoiceMaryTTSImpl v1 =
      new VoiceMaryTTSImpl(PresetVoice.EN_GNOMELIKE);
    VoiceMaryTTSImpl v2 =
      new VoiceMaryTTSImpl(PresetVoice.EN_STORYTELLER);
    VoiceMaryTTSImpl v3 =
      new VoiceMaryTTSImpl(PresetVoice.EN_ADULT_MALE_B);

    // some Utterances
    UtteranceEmotional u3 =
      new UtteranceEmotional(3, "This is a test", SpeechMode.INFORM,
        SituationalMode.HAPPY);
    UtteranceEmotional u4 =
      new UtteranceEmotional(4, "Is this a test?", SpeechMode.QUESTION,
        SituationalMode.BORED);
    Utterance u1 =
      new Utterance(1, "Stop testing please!", SpeechMode.EXCLAMATION);

    AudioInputStream ais1 =
      v1.generateSpeech(u1);
    AudioInputStream ais2 =
      v2.generateSpeech(u3);
    AudioInputStream ais3 =
      v3.generateSpeech(u4);

    new AudioPlayer(ais1).run();
    new AudioPlayer(ais2).run();
    new AudioPlayer(ais3).run();

  }
}
