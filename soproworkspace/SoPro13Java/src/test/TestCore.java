/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package test;

import org.junit.Test;

import tts.script.Dialogue;
import tts.script.Sender;
import tts.script.Utterance;
import tts.script.UtteranceEmotional;
import enums.SituationalMode;
import enums.SpeechMode;

/**
 * {@link TestCore} is a test for classes in {@link tts.script}
 */
public class TestCore {

  @SuppressWarnings("javadoc")
  @Test
  public void test() {

    // some Utterances
    UtteranceEmotional u3 =
      new UtteranceEmotional(3, "This is a test", SpeechMode.INFORM,
        SituationalMode.HAPPY);
    UtteranceEmotional u4 =
      new UtteranceEmotional(4, "Is this is test?", SpeechMode.QUESTION,
        SituationalMode.BORED);
    Utterance u1 =
      new Utterance(1, "Stop testing please!", SpeechMode.EXCLAMATION);

    // two characters
    Sender alpha = new Sender(1);
    alpha.getAttributes().add("young");

    Sender beta = new Sender(2);
    alpha.getAttributes().add("old");
    alpha.getAttributes().add("parent");
    alpha.getAttributes().add("magical");

    Dialogue dia = new Dialogue(1);
    dia.getDialogue().put(u1, alpha);
    dia.getDialogue().put(u3, beta);
    dia.getDialogue().put(u4, alpha);

    System.out.println(dia);

  }
}
