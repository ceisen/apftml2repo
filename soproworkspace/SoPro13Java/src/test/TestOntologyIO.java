/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

/**
 * {@link TestOntologyIO} is used to test the read/write access through OWL API
 */
public class TestOntologyIO {

  @SuppressWarnings("javadoc")
  @Test
  public void write()
      throws OWLOntologyCreationException {

    // load the ontology
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    File fileOnto = new File("SoPro13Python/owl/init.owl");
    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(fileOnto);
    OWLDataFactory factory = manager.getOWLDataFactory();
    PrefixManager pm = new DefaultPrefixManager(null, null, "http://www.sopro13.de/SoPro#");
    assertTrue(ontology.getAxiomCount() == 2374);

    // all axioms will be collected here and applied at the end
    Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();

    // Emotion
    OWLClass emotion = factory.getOWLClass("Emotion", pm);
    // comment - the empty second string makes it untyped
    axioms.add(factory.getOWLAnnotationAssertionAxiom(emotion.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSComment(), factory.getOWLLiteral("some typical emotions", ""))));
    // labels
    axioms.add(factory.getOWLAnnotationAssertionAxiom(emotion.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSLabel(), factory.getOWLLiteral("Emotion", "en"))));
    axioms.add(factory.getOWLAnnotationAssertionAxiom(emotion.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSLabel(), factory.getOWLLiteral("Emotion", "de"))));

    // Love (will be subclass of Emotion)
    OWLClass love = factory.getOWLClass("Love", pm);
    // labels
    axioms.add(factory.getOWLAnnotationAssertionAxiom(love.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSLabel(), factory.getOWLLiteral("Love", "en"))));
    axioms.add(factory.getOWLAnnotationAssertionAxiom(love.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSLabel(), factory.getOWLLiteral("Liebe", "de"))));
    // insert them into the ontoloy
    axioms.add(factory.getOWLSubClassOfAxiom(love, emotion));

    // assume Love has positive connotation and a "target" of the affection
    // first insert a class Connotation below Abstract.
    OWLClass abstractC = factory.getOWLClass("Abstract", pm);
    OWLClass connotation = factory.getOWLClass("Connotation", pm);
    axioms
      .add(factory.getOWLAnnotationAssertionAxiom(
        emotion.getIRI(),
        factory.getOWLAnnotation(factory.getRDFSComment(),
          factory.getOWLLiteral("a subjective interpretation", ""))));
    axioms.add(factory.getOWLAnnotationAssertionAxiom(connotation.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSLabel(), factory.getOWLLiteral("Connotation", "en"))));
    axioms.add(factory.getOWLAnnotationAssertionAxiom(connotation.getIRI(),
      factory.getOWLAnnotation(factory.getRDFSLabel(), factory.getOWLLiteral("Konnotation", "de"))));
    axioms.add(factory.getOWLSubClassOfAxiom(connotation, abstractC));

    // every Emotion has a target, in this case Character.
    // We insert an object property Emotion -> Character
    OWLClass character = factory.getOWLClass("Character", pm);
    OWLObjectProperty hasTarget = factory.getOWLObjectProperty("hasTarget", pm);
    axioms.add(factory.getOWLObjectPropertyDomainAxiom(hasTarget, emotion));
    axioms.add(factory.getOWLObjectPropertyRangeAxiom(hasTarget, character));

    // for fun, we make this symmetrical and reflexive
    axioms.add(factory.getOWLSymmetricObjectPropertyAxiom(hasTarget));
    axioms.add(factory.getOWLReflexiveObjectPropertyAxiom(hasTarget));

    // love is an Emotion with a positive connotation
    // first we need instances for love and positive
    OWLIndividual loveInstance = factory.getOWLNamedIndividual("love", pm);
    axioms.add(factory.getOWLClassAssertionAxiom(emotion, loveInstance));
    OWLIndividual positiveInstance = factory.getOWLNamedIndividual("positive", pm);
    axioms.add(factory.getOWLClassAssertionAxiom(connotation, positiveInstance));

    // and the property
    OWLObjectProperty hasConnotation = factory.getOWLObjectProperty("hasConnotation", pm);
    axioms.add(factory.getOWLObjectPropertyDomainAxiom(hasConnotation, emotion));
    axioms.add(factory.getOWLObjectPropertyRangeAxiom(hasConnotation, connotation));
    axioms.add(factory.getOWLFunctionalObjectPropertyAxiom(hasConnotation));

    // build the triple and add axioms to ontology
    axioms.add(factory.getOWLObjectPropertyAssertionAxiom(hasConnotation, loveInstance, positiveInstance));
    manager.addAxioms(ontology, axioms);
    assertTrue(ontology.getAxiomCount() == 2394);
  }


  @SuppressWarnings("javadoc")
  @Test
  public void read()
      throws OWLOntologyCreationException {

    // load the ontology
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    File fileOnto = new File("SoPro13Python/owl/init.owl");
    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(fileOnto);
    OWLDataFactory factory = manager.getOWLDataFactory();
    PrefixManager pm = new DefaultPrefixManager(null, null, "http://www.sopro13.de/SoPro#");

    // create a reasoner to do the work and check the consistency
    OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
    OWLReasoner reasoner = reasonerFactory.createReasoner(ontology);

    // get all Character instances
    OWLClass character = factory.getOWLClass("Character", pm);
    Set<OWLNamedIndividual> characterInstances = reasoner.getInstances(character, true).getFlattened();
    // for each of those, all referencing expressions (sameAs)
    int referenceCount = 0;
    for (OWLNamedIndividual namedIndividual : characterInstances) {
      Node<OWLNamedIndividual> sameIndividuals = reasoner.getSameIndividuals(namedIndividual);
      referenceCount += sameIndividuals.getSize();
    }

    // ch3_swan_geese -- linked fairyTale
    OWLNamedIndividual ch4 = factory.getOWLNamedIndividual("ft0_ch4", pm);
    OWLObjectProperty isCharacterOf = factory.getOWLObjectProperty("isCharacterOf", pm);
    Set<OWLNamedIndividual> ch4Tales = reasoner.getObjectPropertyValues(ch4, isCharacterOf).getFlattened();

    // all (inferred) superclasses
    Collection<OWLClassExpression> types = EntitySearcher.getTypes(ch4, ontology);

    assertTrue(reasoner.isConsistent());
    assertTrue(ch4Tales.size() == 1);
    assertTrue(types.size() == 2);
    assertTrue(characterInstances.size() == 44);
    assertTrue(referenceCount == 381);
  }
}
