/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.stream.XMLStreamException;

import onto.populate.OntologyPopulator;
import onto.populate.OntologyPopulatorOWLApiImpl;
import onto.populate.OntologyPopulatorXMLParser;
import onto.ref.OntologyRefLookup;
import onto.ref.OntologyRefLookupOWLApiImpl;
import onto.ref.RefLookupXMLWriter;
import onto.script.ScriptExtractorOWLApiImpl;

import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import tts.play.StoryPlayer;
import tts.script.Script;

/**
 * {@link TestOntologyIntegration} is a test class for {@link OntologyPopulator} and {@link OntologyPopulatorXMLParser}.
 */
public class TestOntologyIntegration {

  @SuppressWarnings("javadoc")
  @Test
  public void populate()
      throws OWLOntologyCreationException, IOException, XMLStreamException, OWLOntologyStorageException {

    File input = new File("SoPro13Python/owl/init.owl");
    File output = new File("SoPro13Python/owl/populated.owl");
    OntologyPopulator ontologyPopulator =
      new OntologyPopulatorOWLApiImpl(input, output, "http://www.sopro13.de/SoPro#");
    Path fileXML = Paths.get("SoPro13Python/dialogue/dialogueTests/pythonToOwl.xml");
    OntologyPopulatorXMLParser dialogueXMLParser = new OntologyPopulatorXMLParser(fileXML, ontologyPopulator, 1);
    assertTrue(dialogueXMLParser.parse());
  }


  @SuppressWarnings("javadoc")
  @Test
  public void refLookup()
      throws OWLOntologyCreationException, IOException, XMLStreamException {

    File input = new File("SoPro13Python/owl/init.owl");
    OntologyRefLookup ontologyRefLookup =
      new OntologyRefLookupOWLApiImpl(input, "http://www.sopro13.de/SoPro#");
    FileOutputStream output = new FileOutputStream("./SoPro13Python/emotion/references.xml");
    RefLookupXMLWriter refLookupXMLWriter = new RefLookupXMLWriter(output, ontologyRefLookup, 2);
    assertTrue(refLookupXMLWriter.write());
  }


  @SuppressWarnings("javadoc")
  @Test
  public void populateAndExtract()
      throws OWLOntologyCreationException, IOException, XMLStreamException, OWLOntologyStorageException {

    File input = new File("SoPro13Python/owl/init.owl");
    File output = new File("SoPro13Python/owl/populated.owl");
    String prefix = "http://www.sopro13.de/SoPro#";
    OntologyPopulator ontologyPopulator =
      new OntologyPopulatorOWLApiImpl(input, output, prefix);
    Path fileXML = Paths.get("SoPro13Python/dialogue/dialogueTests/pythonToOwl.xml");
    OntologyPopulatorXMLParser dialogueXMLParser = new OntologyPopulatorXMLParser(fileXML, ontologyPopulator, 1);
    assertTrue(dialogueXMLParser.parse());

    ScriptExtractorOWLApiImpl scriptGenerator = new ScriptExtractorOWLApiImpl(output, prefix);
    Script script = scriptGenerator.getScript(1);
    assertTrue(script.getDialogues().size() > 0);

  }


  @SuppressWarnings("javadoc")
  @Test
  public void playScript()
      throws OWLOntologyCreationException {

    File output = new File("SoPro13Python/owl/populated.owl");
    ScriptExtractorOWLApiImpl scriptGenerator =
      new ScriptExtractorOWLApiImpl(output, "http://www.sopro13.de/SoPro#");
    Script script = scriptGenerator.getScript(1);
    assertTrue(script.getDialogues().size() > 0);
    new StoryPlayer().play(script);

  }


  @SuppressWarnings("javadoc")
  @Test
  public void fullChain()
      throws OWLOntologyCreationException, IOException, XMLStreamException, OWLOntologyStorageException {

    File input = new File("SoPro13Python/owl/init.owl");
    File output = new File("SoPro13Python/owl/populated.owl");
    String prefix = "http://www.sopro13.de/SoPro#";
    OntologyPopulator ontologyPopulator =
      new OntologyPopulatorOWLApiImpl(input, output, prefix);
    Path fileXML = Paths.get("SoPro13Python/dialogue/dialogueTests/pythonToOwl.xml");
    OntologyPopulatorXMLParser dialogueXMLParser = new OntologyPopulatorXMLParser(fileXML, ontologyPopulator, 1);
    assertTrue(dialogueXMLParser.parse());

    ScriptExtractorOWLApiImpl scriptGenerator = new ScriptExtractorOWLApiImpl(output, prefix);
    Script script = scriptGenerator.getScript(1);
    assertTrue(script.getDialogues().size() > 0);
    new StoryPlayer().play(script);

  }
}
