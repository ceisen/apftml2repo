/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package exec;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.stream.XMLStreamException;

import onto.populate.OntologyPopulator;
import onto.populate.OntologyPopulatorOWLApiImpl;
import onto.populate.OntologyPopulatorXMLParser;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 * executes {@link OntologyPopulator}
 */
public final class OntologyPopulatorExecutor {

  /**
   * no instantiation
   */
  private OntologyPopulatorExecutor() {

  };


  /**
   * populates the ontology with information parsed from an specially formatted XML. This uses the in-place version
   * of the {@link OntologyPopulator}.
   * 
   * @param args
   *          <p>
   *          <ul>
   *          <li>args[0] Path to input OWL file to be populated
   *          <li>args[1] namespace IRI prefix
   *          <li>args[2] Path to XML file to be parsed without the file ending
   *          <li>args[3] the fairy tale ID
   *          </ul>
   *          </p>
   * @throws OWLOntologyCreationException
   *           if the populated OWL can not be written
   * @throws XMLStreamException
   *           if the XML file can not be parsed
   * @throws IOException
   *           if the input OWL can not be read
   * @throws OWLOntologyStorageException
   *           internal OWL error
   */
  public static void main(String[] args)
      throws OWLOntologyCreationException, IOException, XMLStreamException, OWLOntologyStorageException {

    File inputOWL = new File(args[0]);
    String prefix = args[1];
    int fairytaleID = Integer.parseInt(args[3]);
    Path fileXML = Paths.get(args[2] + "_ft" + fairytaleID + ".xml");

    System.out.println("populating ontology...");

    OntologyPopulator ontologyPopulator =
      new OntologyPopulatorOWLApiImpl(inputOWL, prefix);
    OntologyPopulatorXMLParser dialogueXMLParser =
      new OntologyPopulatorXMLParser(fileXML, ontologyPopulator, fairytaleID);
    dialogueXMLParser.parse();

    System.out.println("...finished");

  }
}
