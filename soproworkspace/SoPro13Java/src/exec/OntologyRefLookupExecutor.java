/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package exec;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import onto.ref.OntologyRefLookup;
import onto.ref.OntologyRefLookupOWLApiImpl;
import onto.ref.RefLookupXMLWriter;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 * executes {@link OntologyRefLookup}
 */
public final class OntologyRefLookupExecutor {

  /**
   * no instantiation
   */
  private OntologyRefLookupExecutor() {

  };


  /**
   * populated an initial ontology with information parsed from an specially formatted XML
   * 
   * @param args
   *          <p>
   *          <ul>
   *          <li>args[0] Path to the initial OWL file containing the references
   *          <li>args[1] namespace IRI prefix
   *          <li>args[2] Path to the output XML
   *          <li>args[3] the fairy tale ID
   *          </ul>
   *          </p>
   * @throws OWLOntologyCreationException
   *           if the populated OWL can not be written
   * @throws XMLStreamException
   *           if the XML file can not be parsed
   * @throws IOException
   *           if the input OWL can not be read
   */
  public static void main(String[] args)
      throws OWLOntologyCreationException, IOException, XMLStreamException {

    File input = new File(args[0]);
    String prefix = args[1];
    FileOutputStream outputXML = new FileOutputStream(args[2]);
    int fairytaleID = Integer.parseInt(args[3]);

    System.out.println("looking up references...");

    OntologyRefLookup ontologyRefLookup = new OntologyRefLookupOWLApiImpl(input, prefix);

    RefLookupXMLWriter refLookupXMLWriter = new RefLookupXMLWriter(outputXML, ontologyRefLookup, fairytaleID);
    refLookupXMLWriter.write();

    System.out.println("...finished");

  }
}
