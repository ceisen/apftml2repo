/*******************************************************************************
 * Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
 *******************************************************************************/
package exec;

import java.io.File;

import onto.script.ScriptExtractorOWLApiImpl;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import tts.play.StoryPlayer;
import tts.script.Script;

/**
 * executes {@link StoryPlayer}
 */
public final class StoryPlayerExecutor {

  /**
   * no instantiation
   */
  private StoryPlayerExecutor() {

  };


  /**
   * populated an initial ontology with information parsed from an specially formatted XML
   * 
   * @param args
   *          <p>
   *          <ul>
   *          <li>args[0] Path to a OWL file containing dialogue information
   *          <li>args[1] namespace IRI prefix
   *          <li>args[2] the fairy tale ID
   *          </ul>
   *          </p>
   * @throws OWLOntologyCreationException
   *           if the populated OWL can not be written
   */
  public static void main(String[] args)
      throws OWLOntologyCreationException {

    File inputOWL = new File(args[0]);
    String prefix = args[1];
    int fairytaleID = Integer.parseInt(args[2]);

    System.out.println("generating TTS script from ontology...");

    ScriptExtractorOWLApiImpl scriptGenerator = new ScriptExtractorOWLApiImpl(inputOWL, prefix);
    Script script = scriptGenerator.getScript(fairytaleID);

    System.out.println("...finished");

    new StoryPlayer().play(script);

  }

}
