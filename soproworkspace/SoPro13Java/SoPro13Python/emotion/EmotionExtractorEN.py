################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

"""
@author: Jana Ott
"""

from nltk.corpus import wordnet


class EmotionExtractorEN():
    """EmotionExtractor to create a list of words using nltk wordnet"""

    def __init__(self):
        """constructor:
        emolist: list of words, their synsets and subsynsets"""
        self.emolist = []

    def getEmoSynsets(self, wordlist, pos):
        """takes a list of seedwords and their postag, uses getEmoList
        to get their synsets and adds them to emolist """
        for word in wordlist:
            emoSynsets = wordnet.synsets(word, pos.lower())  # @UndefinedVariable
            synset = self.getEmoList(emoSynsets, pos, [])
            self.emolist.extend(synset)
        return self.emolist

    def getEmoList(self, emoSynsets, pos, emolist):
        """takes synsets, their postag and a list and adds recursively all
        found words to this emolist"""
        if pos == "N":  # adds hyponym synsets for nouns
            for s in emoSynsets:
                for name in s.lemma_names:
                    name = name.replace("_", " ")
                    if name not in  emolist:
                        emolist.append(name)
                self.getEmoList(s._related('~'), pos, emolist)
        if pos == "A":  # adds similar synsets for adjectives
            for s in emoSynsets:
                for name in s.lemma_names:
                    name = name.replace("_", " ")
                    if name not in emolist:
                        emolist.append(name)
                for similar in s._related('&'):  # no recursion necessary
                    for name in similar.lemma_names:
                        name = name.replace("_", " ")
                        if name not in emolist:
                            emolist.append(name)
        if pos == "V":  # adds hyponym synsets for verbs
            for s in emoSynsets:
                for name in s.lemma_names:
                    name = name.replace("_", " ")
                    if name not in  emolist:
                        emolist.append(name)
                self.getEmoList(s._related('~'), pos, emolist)
                """for hypo in s._related('~'):
                    for name in hypo.lemma_names:
                        if name not in emolist:
                            emolist.append(name)"""
        return emolist
