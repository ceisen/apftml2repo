# -*- coding: utf-8 -*-

################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

"""
@author: Jana Ott
"""
import pattern.text.en as pattern
from EmotionExtractorDE import EmotionExtractorDE
from EmotionExtractorEN import EmotionExtractorEN


class EmotionCollector():
    """takes a language ("de"=German, "en"=English) and seedwords
    (lists with tuples containing a list of words, their postag and their
    emotion) and uses EmotionExtractors to create a dictionary which maps
    emotions to words"""

    def __init__(self, lang, seedwords):
        """constructor:
        lang: language ("de" or "en")
        dict: dictionary that will map sets of emotions to words
        seedwords: lists with tuples containing a list of words,
        their postag and their emotion"""
        self.lang = lang
        self.dict = {}
        self.seedwords = seedwords

    def addSeedwords(self, seedwords):
        """adds additional seedwords"""
        self.seedwords.extend(seedwords)

    def addEntry(self, wordlist, pos, emo):
        """adds additional words and their emotion without synsets"""
        if pos == "A":  # adds comparative and superlative, because pattern lemmatizer doesn't work on them
            lex = []
            for word in wordlist:
                comp = pattern.comparative(word)
                if " " not in comp:
                    lex.append(comp)
                sup = pattern.superlative(word)
                if " " not in sup:
                    lex.append(sup)
            wordlist.extend(lex)
        for word in wordlist:
            if word in self.dict:
                self.dict[word].add(emo)
            else:
                self.dict[word] = set([emo])

    def createDict(self):
        """uses EmotionExtractors and seedwords to create a dictionary"""
        for (wordlist, pos, emo) in self.seedwords:
            if self.lang == "de":
                eddie = EmotionExtractorDE()
                erg = eddie.getEmolist(wordlist)
            if self.lang == "en":
                enie = EmotionExtractorEN()
                erg = enie.getEmoSynsets(wordlist, pos)
                if pos == "A":  # adds comparative and superlative, because pattern lemmatizer doesn't work on them
                    lex = []
                    for word in erg:
                        comp = pattern.comparative(word)
                        if " " not in comp:
                            lex.append(comp)
                        sup = pattern.superlative(word)
                        if " " not in sup:
                            lex.append(sup)
                    erg.extend(lex)
            for word in erg:
                if word not in self.dict:
                    self.dict[word] = set([emo])
                else:
                    self.dict[word].add(emo)
        return self.dict

    def getDict(self):
        """returns the dictionary"""
        return self.dict
