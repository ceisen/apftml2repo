# -*- coding: utf-8 -*-

################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

"""
@author: Jana Ott
"""

import urllib2
import xml.sax as sax


class EmoHandler(sax.handler.ContentHandler):
    """Sax-parser that extracts synsets and their subsynsets
    from openthesaurus.de and puts them into emolist """

    def __init__(self):
        """ constructor:
        emolist: list of all words found in a synset and its subsynsets
        word: newly found word
        """
        self.emolist = []
        self.word = ""

    def startElement(self, name, attrs):
        """ sets word to the found term"""
        if name == "term":
            self.word = attrs["term"]

    def endElement(self, name):
        """removes information in parentheses and adds the term to emolist"""
        if name == "term":
            par = False
            newword = ""
            for c in self.word:
                if not par:
                    if c == "(":
                        par = True
                    else:
                        newword += c
                else:
                    if c == ")":
                        par = False
            self.emolist.append(newword.encode("utf8"))


class EmotionExtractorDE():
    """EmotionExtractor to create a list of words using openthesaurus.de """

    def __init__(self):
        """constructor:
        emolist: list of words, their synsets and subsynsets"""
        self.emolist = []

    def getEmolist(self, wordlist):
        """ adds for each word in wordlist its synset and
        subsynsets to emolist"""
        for word in wordlist:
            url = "http://www.openthesaurus.de/synonyme/search?q=" + word + "&format=text/xml&subsynsets=true"
            response = urllib2.urlopen(url)
            handler = EmoHandler()
            parser = sax.make_parser()
            parser.setContentHandler(handler)
            parser.parse(response)
            synset = handler.emolist
            self.emolist.extend(synset)
        return self.emolist
