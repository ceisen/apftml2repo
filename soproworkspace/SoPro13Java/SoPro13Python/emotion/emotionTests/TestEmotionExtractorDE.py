################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 17.05.2014

@author: Jana Ott
'''
import unittest
from emotion.EmotionExtractorDE import EmotionExtractorDE


class TestExtractor(unittest.TestCase):
    """ class for testing the German EmotionExtractor"""

    def testExtractor(self):
        """test on example words, prints two versions to check encoding"""
        eddie = EmotionExtractorDE()
        erg = eddie.getEmolist(["Gartenzaun", "Blume"])
        print erg
        for word in erg:
            print word
