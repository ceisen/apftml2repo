################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 17.05.2014

@author: Jana
'''
import unittest
from emotion.EmotionExtractorEN import EmotionExtractorEN


class TestExtractor(unittest.TestCase):
    """class for testing the English EmotionExtractor"""

    def testExtractor(self):
        """test on example words"""
        enie = EmotionExtractorEN()
        erg = enie.getEmoSynsets(["fence", "cauliflower"], "N")
        print erg
