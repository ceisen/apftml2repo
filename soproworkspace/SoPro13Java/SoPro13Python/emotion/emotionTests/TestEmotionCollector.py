# -*- coding: utf-8 -*-

################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 17.05.2014

@author: Jana Ott
'''
import unittest
from emotion.EmotionCollector import EmotionCollector


class TestCollector(unittest.TestCase):
    """class for testing EmotionCollector"""

    def testCollectorDE(self):
        """test vor the German version"""
        de_collection = EmotionCollector("de", [(["Gartenzaun", "Blume"], "N", "neutral")])
        coll = de_collection.createDict()
        for entry in coll:
            print entry
        print ("Orchideengewächs" in coll)
        print coll

    def testCollectorEN(self):
        """test for the English version"""
        seedcollection = [(["happy", "pleased"], "A", "happy"),
                      (["gladden", "jubilate", "enjoy"], "V", "happy"),
                      (["joy", "happiness"], "N", "happy"),
                      (["angry", "disgusting"], "A", "angry"),
                      (["hate"], "V", "angry"),
                      (["anger", "disgust"], "N", "angry"),
                      (["afraid"], "A", "frightened"),
                      (["fear"], "V", "frightened"),
                      (["fear"], "N", "frightened"),
                      (["sad"], "A", "sad"),
                      (["grieve", "cry", "lament", "sadden"], "V", "sad"),
                      (["sadness"], "N", "sad"),
                      (["surprised"], "A", "surprised"),
                      (["surprise"], "V", "surprised"),
                      (["surprise"], "N", "surprised")]
        en_collection = EmotionCollector("en", seedcollection)
        en_collection.addEntry(["weak"], "A", "weak")
        coll = en_collection.createDict()
        if "frightened" in coll:
            print coll["frightened"]
        else:
            print "not here"
        print coll.has_key("happy") == coll.has_key("happier")
        print coll.has_key("happy") == coll.has_key("happiest")
        print len(coll)
