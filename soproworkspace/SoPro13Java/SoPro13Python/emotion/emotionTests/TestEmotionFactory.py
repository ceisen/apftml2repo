################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 17.05.2014

@author: Jana Ott
'''
import sys
sys.path.insert(0, './lib/nltk-2.0.1rc1-py2.6.egg')
sys.path.insert(0, './time')
sys.path.append('./emotion')
sys.path.append('./dialogue')
sys.path.append('./dialogue/XML')
sys.path.append('./lib')
import unittest
from emotion.EmotionFactory import EmoFactory
from dialogue.utteranceRep import Utterance
from dialogue.dialogueRep import Dialogue
from timeObjects import timeObject


class TestEmotionFactory(unittest.TestCase):
    """class for testing the EmotionFactory"""

    def testExamples(self):
        """examples to show how EmotionFactory works"""
        dialogues = []
        t1 = timeObject(1, "", "")
        t2 = timeObject(2, "", "")
        t3 = timeObject(3, "", "")
        t4 = timeObject(4, "", "")
        t5 = timeObject(5, "", "")
        t6 = timeObject(6, "", "")
        t7 = timeObject(7, "", "")
        t8 = timeObject(8, "", "")
        t9 = timeObject(9, "", "")
        t10 = timeObject(10, "", "")
        u1 = Utterance(t1, "I have never been so happy.", "narrator", "reader", {})
        u2 = Utterance(t2, "He wasn't happy at all.", "narrator", "reader", {})
        u3 = Utterance(t3, "He was never happy and sometimes he got very angry.", "narrator", "reader", {})
        u4 = Utterance(t4, "There was nothing but sadness in his heart.", "narrator", "reader", {})
        d1 = Dialogue([u1, u2, u3, u4], [], [])  # examples for negated emotions
        dialogues.append(d1)
        u5 = Utterance(t5, "The king said with an angry voice to his daughter", "narrator", "reader", {})
        u6 = Utterance(t5, "Don't do that!", "the king", "his daughter", {})
        d2 = Dialogue([u5, u6], [], [])
        dialogues.append(d2)
        u7 = Utterance(t6, "Why did you do that?", "the princess", "her father", {})
        u8 = Utterance(t6, "You cannot sit next to me,", "his daughter", "her father", {})  # sender=same character as in u9, but different refference
        u9 = Utterance(t6, "cried the princess.", "narrator", "reader", {})
        d3 = Dialogue([u7, u8, u9], [], [])
        dialogues.append(d3)
        u10 = Utterance(t7, "cockadoodledoo", "the frog", "the king", {})
        u11 = Utterance(t7, "With happiness in his voice the frog pretended to be the king.", "narrator", "reader", {}) # emo noun, the frog as part of the subject
        d4 = Dialogue([u10, u11], [], [])
        dialogues.append(d4)
        u12 = Utterance(t8, "Why are you so sad?", "the frog", "the princess", {})
        u13 = Utterance(t8, "asked the frog.", "narrator", "reader", {})
        u14 = Utterance(t9, "Leave me alone!", "she", "the frog", {})
        d5 = Dialogue([u12, u13, u14], [], [])
        dialogues.append(d5)
        u15 = Utterance(t10, "This is a part of a sentence without any emotion,", "narrator", "reader", {})
        u16 = Utterance(t10, "but this very frightening part of the sentence...", "narrator", "reader", {})
        u17 = Utterance(t10, "affects the whole sentence...", "narrator", "reader", {})
        u18 = Utterance(t10, "except for parts with their own emotion like happiness", "narrator", "reader", {})
        d6 = Dialogue([u15, u16, u17, u18], [], [])
        dialogues.append(d6)
        emo = EmoFactory(dialogues, references=False)
        references = [("ch0", ["the king", "he_0", "her father"]),("ch1", ["the frog", "he_1", "the frog prince"]),("ch2", ["the princess", "she", "her", "his daughter"])]
        emo.setReferences(references)
        emo.setEmo()
        for dialogue in dialogues:
            print "Dialogue", dialogue.getID()
            for utterance in dialogue.utterances:
                print(utterance.getEmotion(), utterance.getText())

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
