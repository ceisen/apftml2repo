################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 17.05.2014

@author: Jana Ott
'''
import sys
sys.path.insert(0, './lib/nltk-2.0.1rc1-py2.6.egg')
sys.path.insert(0, './time')
sys.path.append('./emotion')
sys.path.append('./dialogue')
sys.path.append('./dialogue/XML')
sys.path.append('./lib')
import unittest
from emotion.EmotionFactory import EmoFactory
from timeParser import TimeParser
from dialogue.dialogueExtractor import DialogueExtractor


class TestFairyTalesEmotion(unittest.TestCase):
    """class for testing the EmotionFactory on Fairy Tales"""

    def testFairyTales(self):
        """prints utterances with their emotion after getFirstEmo to check
        whether there are any words missing/wrong in emodict"""
        tparser = TimeParser()
        # filenames: The-Swan-Geese.txt, The-Bremen-Town-Musicians.txt, The-Frog-King.txt, Rumpelstiltskin.txt
        tstructure = tparser.parse('The-Frog-King.txt')
        XMLTimeLine = []
        ParsingTimeLine = []
        for i in tstructure.structure:
            XMLTimeLine.append(tstructure.structure[i])
            ParsingTimeLine.append(tstructure.structure[i][0])
        extractor = DialogueExtractor()
        extractor.parseTimeLine(ParsingTimeLine)
        dialogues = extractor.dialogues
        print "start emotion extraction..."
        emFactory = EmoFactory(dialogues, references=False)
        emFactory.setReferences("references_test_fk.xml") #fk: Frog-King, rs: Rumpelstiltskin, btm: Bremen-Town-Musicians
        change = {}
        emFactory.setFirstEmo()
        emFactory.decideEmo()
        for dialogue in dialogues:
            for utterance in dialogue.utterances:
                change[utterance.getID()] = [utterance.getEmotion()]
        emFactory.deleteNegEmo()
        emFactory.decideEmo()
        for dialogue in dialogues:
            for utterance in dialogue.utterances:
                change[utterance.getID()].append(utterance.getEmotion())
        emFactory.moveNarratorEmo()
        emFactory.decideEmo()
        for dialogue in dialogues:
            for utterance in dialogue.utterances:
                change[utterance.getID()].append(utterance.getEmotion())
        emFactory.moveDiaEmo()
        emFactory.decideEmo()
        for dialogue in dialogues:
            for utterance in dialogue.utterances:
                change[utterance.getID()].append(utterance.getEmotion())
        emFactory.spreadEmo()
        for dialogue in dialogues:
            for utterance in dialogue.utterances:
                change[utterance.getID()].append(utterance.getEmotion())
            for utterance in dialogue.utterances:
                print utterance.getTime().getId(), change[utterance.getID()], utterance.getText()
        print "...finished"

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
