################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

"""
@author: Jana Ott
"""

import os.path
import re
import xml.sax as sax
import pattern.text.en as pattern
import nltk
from EmotionCollector import EmotionCollector


class ReferenceHandler(sax.handler.ContentHandler):
    """ Sax-Parser to get references and map them to their character"""

    def __init__(self):
        """constructor:
        referencedict: dictionary to map references to characters
        char: fairy-tale character"""
        self.referencedict = {}
        self.char = ""

    def startElement(self, name, attrs):
        """adds a reference to a set of references mapped to a character,
        numbers indicating the fairy-tale and underlines are removed"""
        if "ref" in attrs:
            self.char = attrs["ref"]
            numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
            lineflag = False
            newname = ""
            for character in name:
                if character == "_":
                    lineflag = True
                elif lineflag and character not in numbers:
                    lineflag = False
                    newname += (" " + character)
                elif lineflag:
                    lineflag = False
                    newname += ("_" + character)
                else:
                    newname += character
            #newname = newname.replace("_[0-9]+", "", 1)
            newname = re.sub(r'_[0-9]+', "", newname, count=1)
            if self.char in self.referencedict:
                self.referencedict[self.char].add(newname)
            else:
                self.referencedict[self.char] = set([newname])

    def endElement(self, name):
        """sets character back to '' """
        self.char = ""


class EmoFactory():
    """class to set an emotion to every utterance in a list of dialogues,
    takes references.xml by default to get character references
    """
    def __init__(self, dialogues, references=True):
        """constructor:
        emocollection: emotionwords mapped to emotions
        dialogues: list of dialogues from a fairy-tale
        emodict: emotions mapped to wordpositions
        refdict: references mapped to characters
        """
        seedcollection = [(["happy", "pleased", "content"], "A", "happy"),
                          (["gladden", "jubilate", "enjoy"], "V", "happy"),
                          (["joy", "happiness"], "N", "happy"),
                          (["angry", "disgusting"], "A", "angry"),
                          (["hate"], "V", "angry"),
                          (["anger", "disgust"], "N", "angry"),
                          (["afraid", "frightening"], "A", "frightened"),
                          (["fear", "frighten"], "V", "frightened"),
                          (["fear"], "N", "frightened"),
                          (["sad"], "A", "sad"),
                          (["grieve", "cry", "lament", "sadden"], "V", "sad"),
                          (["sadness"], "N", "sad"),
                          (["surprised"], "A", "surprised"),
                          (["surprise"], "V", "surprised"),
                          (["surprise"], "N", "surprised")]
        emo_collection = EmotionCollector("en", seedcollection)
        emo_collection.addEntry(["weak", "tired"], "A", "weak")
        emo_collection.addEntry(["weaken"], "V", "weak")
        emo_collection.addEntry(["weakness"], "S", "weak")
        emo_collection.addEntry(["suffer"], "V", "sad")
        self.emocollection = emo_collection.createDict()
        if "golden" in self.emocollection:
            del self.emocollection["golden"]
        if "catch" in self.emocollection:
            del self.emocollection["catch"]
        if "call" in self.emocollection:
            del self.emocollection["call"]
        if "care" in self.emocollection:
            del self.emocollection["care"]
        if "blow" in self.emocollection:
            del self.emocollection["blow"]
        self.dialogues = dialogues
        self.emodict = {}
        if references:
            handler = ReferenceHandler()
            parser = sax.make_parser()
            parser.setContentHandler(handler)
            __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
            parser.parse(os.path.join(__location__, "references.xml"))
            self.refdict = handler.referencedict
        else:
            self.refdict = {}
        #print self.refdict

    def setFirstEmo(self):
        """finds emotionwords and adds their emotion to emodict"""
        for dialogue in self.dialogues:
            for utterance in dialogue.utterances:
                ID = utterance.getID()
                self.emodict[ID] = {}
                pointutterance = utterance.getText().replace(".", " .")
                pt = pattern.parsetree(pointutterance, relations=True, lemmata=True)
                lemmalist = []
                for sentence in pt:
                    lemmalist.extend(sentence.lemmata)
                position = 0
                for word in lemmalist:
                    if word in self.emocollection:
                        self.emodict[ID][position] = []
                        for emo in self.emocollection[word]:
                            self.emodict[ID][position].append(emo)
                    position += 1

    def deleteNegEmo(self):
        """deletes emotions from emodict that are negated"""
        negationcollection = ["no", "not", "n't", "nothing", "never", "nowhere", "neither"]
        exceptcollection = ["except", "but"]
        for dialogue in self.dialogues:
            for utterance in dialogue.utterances:
                ID = utterance.getID()
                if self.emodict[ID] != {}:  # only utterances with emotionwords
                    pointutterance = utterance.getText().replace(".", " .")
                    tokenlist = nltk.word_tokenize(pointutterance)
                    negflag = False
                    for token in tokenlist:
                        if token in negationcollection:
                            negflag = True
                    if negflag:  # only utterances with negationwords
                        tree = pattern.parsetree(pointutterance, relations=True, lemmata=True)
                        sentencecounter = 0  # ##
                        for sentence in tree.sentences:
                            for chunk in sentence.chunks:  # chunks
                                chunkstring = chunk.string
                                chunktl = nltk.word_tokenize(chunkstring)
                                negflag = False
                                exceptflag = False
                                for token in chunktl:
                                    if token in negationcollection:
                                        negflag = True
                                    elif token in exceptcollection:
                                        exceptflag = True
                                if negflag and not exceptflag:  # only chunks with negationwords
                                    chunklemmalist = chunk.lemmata
                                    position = sentencecounter + chunk.start
                                    for lemma in chunklemmalist:
                                        if lemma in self.emocollection:
                                            self.emodict[ID][position] = []
                                        position += 1
                            sentencecounter += sentence.stop

    def moveNarratorEmo(self):
        """transfers emotions from the narrator to the referred character"""
        numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        for dialogue in self.dialogues:
            for utterance in dialogue.utterances:
                sender = utterance.getSender()
                ID = utterance.getID()
                if self.emodict[ID] != {} and sender == "narrator":
                    timeID = utterance.getTime().getId()
                    pointutterance = utterance.getText().replace(".", " .")
                    for charutterance in dialogue.utterances:
                        if charutterance.getTime().getId() == timeID and charutterance.getID() != ID: #not the same utterance but at the same time
                            tree = pattern.parsetree(pointutterance, relations=True, lemmata=True)
                            sentencecounter = 0
                            for sentence in tree.sentences:
                                wordcounter = sentencecounter
                                for word in sentence.words:
                                    if wordcounter in self.emodict[ID]:
                                        if word.type.startswith("NN"):
                                            found = False
                                            wrong = False
                                            emo = set([])
                                            for char in self.refdict:  # checks if only the sender is mentioned in the sentence
                                                for ref in self.refdict[char]:
                                                    number = False
                                                    newref = ref
                                                    while newref[-1] in numbers:
                                                        number = True
                                                        newref = newref[:-1]
                                                    if number:
                                                        newref = newref[:-1]
                                                    if newref in sentence.string:
                                                        if charutterance.getSender() in self.refdict[char]:
                                                            emo = self.emodict[ID][wordcounter]
                                                        else:
                                                            wrong = True
                                            if emo != set([]) and not wrong:  # only if no other character is in the utterance
                                                self.emodict[charutterance.getID()][-2] = emo
                                                found = True
                                            if not found:  # check if the sender or a wrong character is in the same chunk
                                                chunk = word.chunk
                                                for char in self.refdict:
                                                    for ref in self.refdict[char]:
                                                        number = False
                                                        newref = ref
                                                        while newref[-1] in numbers:
                                                            number = True
                                                            newref = newref[:-1]
                                                        if number:
                                                            newref = newref[:-1]
                                                        if newref in chunk.string:
                                                            found = True
                                                            if charutterance.getSender() in self.refdict[char]:
                                                                self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                                if not found:  # check if the sender or a wrong character is subject
                                                    for sub in sentence.subjects:
                                                        for char in self.refdict:
                                                            for ref in self.refdict[char]:
                                                                number = False
                                                                newref = ref
                                                                while newref[-1] in numbers:
                                                                    number = True
                                                                    newref = newref[:-1]
                                                                if number:
                                                                    newref = newref[:-1]
                                                                if newref in sub.string:
                                                                    found = True
                                                                    if charutterance.getSender() in self.refdict[char]:
                                                                        self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                        elif word.type.startswith("JJ"):
                                            found = False
                                            chunk = word.chunk
                                            for char in self.refdict:
                                                for ref in self.refdict[char]:
                                                    number = False
                                                    newref = ref
                                                    while newref[-1] in numbers:
                                                        number = True
                                                        newref = newref[:-1]
                                                    if number:
                                                        newref = newref[:-1]
                                                    if newref in chunk.string:
                                                        found = True
                                                        if charutterance.getSender() in self.refdict[char]:
                                                            self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                            if not found:  # adjectiv not together with reference or wrong reference
                                                make = False
                                                for verb in sentence.verbs:
                                                    for word in verb.words:
                                                        if word.lemma == "make":
                                                            make = True
                                                if make:
                                                    for obj in sentence.objects:
                                                        for char in self.refdict:
                                                            for ref in self.refdict[char]:
                                                                number = False
                                                                newref = ref
                                                                while newref[-1] in numbers:
                                                                    number = True
                                                                    newref = newref[:-1]
                                                                if number:
                                                                    newref = newref[:-1]
                                                                if newref in obj.string:
                                                                    if charutterance.getSender() in self.refdict[char]:
                                                                        self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                                else:
                                                    for sub in sentence.subjects:
                                                        for char in self.refdict:
                                                            for ref in self.refdict[char]:
                                                                number = False
                                                                newref = ref
                                                                while newref[-1] in numbers:
                                                                    number = True
                                                                    newref = newref[:-1]
                                                                if number:
                                                                    newref = newref[:-1]
                                                                if newref in sub.string:
                                                                    if charutterance.getSender() in self.refdict[char]:
                                                                        self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                        elif word.type.startswith("RB"):
                                            for sub in sentence.subjects:
                                                for char in self.refdict:
                                                    for ref in self.refdict[char]:
                                                        number = False
                                                        newref = ref
                                                        while newref[-1] in numbers:
                                                            number = True
                                                            newref = newref[:-1]
                                                        if number:
                                                            newref = newref[:-1]
                                                        if newref in sub.string:
                                                            if charutterance.getSender() in self.refdict[char]:
                                                                self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                        elif word.type.startswith("VB"):
                                            if word.chunk.start == 0:  # if the sentence starts with a verb, as in "...", said xy
                                                if word.chunk.object != None:
                                                    for char in self.refdict:
                                                        for ref in self.refdict[char]:
                                                            number = False
                                                            newref = ref
                                                            while newref[-1] in numbers:
                                                                number = True
                                                                newref = newref[:-1]
                                                            if number:
                                                                newref = newref[:-1]
                                                            if newref in word.chunk.object.string:
                                                                if charutterance.getSender() in self.refdict[char]:
                                                                    self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                            else:
                                                for sub in sentence.subjects:
                                                    for char in self.refdict:
                                                        for ref in self.refdict[char]:
                                                            number = False
                                                            newref = ref
                                                            while newref[-1] in numbers:
                                                                number = True
                                                                newref = newref[:-1]
                                                            if number:
                                                                newref = newref[:-1]
                                                            if newref in sub.string:
                                                                if charutterance.getSender() in self.refdict[char]:
                                                                    self.emodict[charutterance.getID()][-2] = self.emodict[ID][wordcounter]
                                    wordcounter += 1
                                sentencecounter += sentence.stop

    def moveDiaEmo(self):
        """transfers emotions that refer to the dialogue partner"""
        for dialogue in self.dialogues:
            for utterance in dialogue.utterances:
                sender = utterance.getSender()
                ID = utterance.getID()
                if self.emodict[ID] != {} and sender != "narrator":  # only utterances with emotionwords
                    pointutterance = utterance.getText().replace(".", " .")
                    tokenlist = nltk.word_tokenize(pointutterance)
                    youflag = False
                    for token in tokenlist:
                        if token == "you" or token == "your":
                            youflag = True
                    if youflag:
                        tree = pattern.parsetree(pointutterance, relations=True, lemmata=True)
                        sentencecounter = 0
                        for sentence in tree.sentences:
                            wordcounter = sentencecounter
                            for word in sentence.words:
                                if wordcounter in self.emodict[ID]:
                                    if word.type.startswith("NN"):
                                        chunk = word.chunk
                                        for chunkword in chunk.words:
                                            if chunkword.string == "you" or chunkword.string == "your":
                                                self.diaHilf(dialogue, utterance, wordcounter)
                                    if word.type.startswith("JJ"):
                                        jjflag = False
                                        chunk = word.chunk
                                        if jjflag == False:
                                            if chunk.type == "ADJP" or chunk.role == "OBJ":  # you are sad or you are a sad girl
                                                for areword in sentence:
                                                    if areword.string == "are":
                                                        arechunk = areword.chunk
                                                        if len(arechunk.words) == 1:  # only are in chunk
                                                            self.diaHilf(dialogue, utterance, wordcounter)
                                    if word.type.startswith("RB"):
                                        if sentence.words[-1] == "?":
                                            obs = sentence.objects
                                            obflag = False
                                            for ob in obs:
                                                for word in ob:
                                                    if word == "you":
                                                        obflag = True
                                            if obflag:
                                                self.diaHilf(dialogue, utterance, wordcounter)
                                        else:
                                            subs = sentence.subjects
                                            subflag = False
                                            for sub in subs:
                                                for word in sub:
                                                    if word == "you":
                                                        subflag = True
                                            if subflag:
                                                self.diaHilf(dialogue, utterance, wordcounter)
                                    if word.type.startswith("VB"):
                                        if sentence.words[-1].string == "?":
                                            obs = sentence.objects
                                            obflag = False
                                            for ob in obs:
                                                for word in ob.words:
                                                    if word.string == "you":
                                                        obflag = True
                                                if len(ob.words) == 1:
                                                    for word in ob.words:
                                                        if word.string == "your":
                                                            obflag = True
                                            if obflag:
                                                self.diaHilf(dialogue, utterance, wordcounter)
                                        else:
                                            subs = sentence.subjects
                                            subflag = False
                                            for sub in subs:
                                                for word in sub:
                                                    if word == "you":
                                                        subflag = True
                                                if len(sub.words) == 1:
                                                    for word in sub.words:
                                                        if word.string == "your":
                                                            subflag = True
                                            if subflag:
                                                self.diaHilf(dialogue, utterance, wordcounter)
                                wordcounter += 1
                            sentencecounter += sentence.stop

    def diaHilf(self, dialogue, emoutterance, count):
        """helper for moveDiaEmo to find out where to transfer the emotion"""
        emoutteranceID = emoutterance.getID()
        newtimeID = -1
        rec = ""
        sender = ""
        for entry in self.refdict:
            if emoutterance.getReceiver() in self.refdict[entry]:
                rec = entry
            elif emoutterance.getSender() in self.refdict[entry]:
                sender = entry
        if rec == "":  # wrong ("Wait") or no receiver ("receiver")
            newsender = ""
            foundreceiver = False
            knownreceiver = False
            for utterance in dialogue.utterances:
                if utterance.getID() > emoutteranceID:
                    if not foundreceiver:
                        if utterance.getSender() != "narrator" and utterance.getSender() not in self.refdict[sender]:  # not equal to sender or narrator
                            newsender = utterance.getSender()
                            newtimeID = utterance.getTime().getId()
                            foundreceiver = True
                            self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
                            for entry in self.refdict:
                                if newsender in self.refdict[entry]:
                                    newsender = entry
                                    knownreceiver = True
                    else:  # receiver already found
                        if knownreceiver:
                            if utterance.getSender() in self.refdict[newsender]:
                                if utterance.getTime().getId() == newtimeID:
                                    self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
                        else:
                            if utterance.getSender() == newsender:
                                if utterance.getTime().getId() == newtimeID:
                                    self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
            self.emodict[emoutteranceID][count] = []
        else:  # receiver identified
            answered = False  # identified receiver has already answered
            for utterance in dialogue.utterances:
                if not answered:
                    if utterance.getID() > emoutteranceID:
                        if utterance.getSender() in self.refdict[rec]:
                            answered = True
                            newtimeID = utterance.getTime().getId()
                            self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
                else:
                    if utterance.getSender() in self.refdict[rec] and utterance.getTime().getId() == newtimeID:
                        self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
            if answered == False:  # identified receiver did not answer
                newsender = ""
                for utterance in dialogue.utterances:
                    if not answered:
                        if utterance.getID() > emoutteranceID and utterance.getSender != "narrator":
                            newsender = utterance.getSender()
                            for entry in self.refdict:
                                if utterance.getSender in self.refdict[entry]:
                                    newsender = ""
                            if newsender != "":
                                answered = True
                                self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
                    else:
                        if utterance.getSender == newsender and utterance.getTime().getId() == newtimeID:
                            self.emodict[utterance.getID()][-1] = self.emodict[emoutteranceID][count]
            self.emodict[emoutteranceID][count] = []

    def setReferences(self, references):
        """for testing, sets the references manually, references from xml or
        as a list with tuples: [(char0, [ref0, ref1,...]),(),...]"""
        if type(references) is str:
            if references.endswith(".xml"):
                handler = ReferenceHandler()
                parser = sax.make_parser()
                parser.setContentHandler(handler)
                __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
                parser.parse(os.path.join(__location__, references))
                self.refdict = handler.referencedict
        elif type(references) is list:
            for entry in references:
                self.refdict[entry[0]] = set(entry[1])

    def spreadEmo(self):
        """adds emotions to utterances without emotions but the same timeID"""
        timedict = {}
        for dialogue in self.dialogues:
            for utterance in dialogue.utterances:
                if utterance.getTime().getId() in timedict:
                    timedict[utterance.getTime().getId()].append(utterance)
                else:
                    timedict[utterance.getTime().getId()] = [utterance]
        for time in timedict:
            timelist = []
            for utterance in timedict[time]:
                if utterance.getEmotion() != "neutral":
                    timelist.append(utterance)
            for utterance in timedict[time]:
                if len(timelist) > 0:
                    if utterance.getID() < timelist[0].getID() and utterance.getSender() == timelist[0].getSender():
                        if utterance.getEmotion() == "neutral":
                            utterance.setEmotion(timelist[0].getEmotion())
            while len(timelist) > 0:
                for utterance in timedict[time]:
                    if utterance.getID() > timelist[-1].getID() and utterance.getSender() == timelist[-1].getSender():
                        if utterance.getEmotion() == "neutral":
                            utterance.setEmotion(timelist[-1].getEmotion())
                timelist = timelist[:-1]

    def decideEmo(self):
        """decides which emotion to map to the utterance"""
        for dialogue in self.dialogues:
            for utterance in dialogue.utterances:
                ID = utterance.getID()
                if self.emodict[ID] == {}:  # no emotion
                    utterance.setEmotion("neutral")
                else:
                    countdict = {}
                    for entry in self.emodict[ID]:
                        for emo in self.emodict[ID][entry]:
                            if emo in countdict:
                                countdict[emo] += 1
                            else:
                                countdict[emo] = 1
                    emo = "neutral"
                    counter = 0
                    for entry in countdict:
                        if countdict[entry] > counter:  # if they are the same, the first one is picked "randomly"
                            counter = countdict[entry]
                            emo = entry
                    utterance.setEmotion(emo)

    def setEmo(self):
        """combines all steps to set emotions and decides which one to map
        to the utterance"""
        self.setFirstEmo()
        self.deleteNegEmo()
        self.moveNarratorEmo()
        self.moveDiaEmo()
        self.decideEmo()
        self.spreadEmo()
