################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 08.03.2014

@author: Christian Willms
'''

'''representation of an utterance'''
class Utterance:
    
    UtteranceID = 0
    
    
    def __init__(self,time,utteranceText,sender,receiver,pronouns ):
        '''
        Constructor:
        time: the time of the utterance
        utteranceText: the span of the utterance
        utteranceType: type of the utterance (question, answer, salutation,...)
        sender: the sender
        receiver: the receiver
        emotion: the emotion, added later by setter
        pronouns: dictionary with the actual state of the pronouns indexes 
        '''
        self.id = Utterance.UtteranceID
        self.time = time
        self.utteranceText = utteranceText
        self.utteranceType = self.extractUtteranceType()
        self.sender = sender
        self.receiver = receiver
        self.emotion = "default"
        self.pronouns = pronouns
        Utterance.UtteranceID +=1
        
         
    def extractUtteranceType(self): 
        '''
        This method is used to extract the UtteranceType 
        '''  
        if "!" in self.utteranceText:
                return "exclamation"
        elif "answered" in self.time.getSpan():            #utteranceText:
                return "answer"
        elif "thought" in self.time.getSpan():
                return "monologue"
        elif "?" in self.utteranceText or "asked" in self.utteranceText:
                return "question"
        
        else:
            return "inform"
        
### Getter and Setter ### 
    def getText(self):
        return self.utteranceText
            
    def getTime(self):
        return self.time
    
    def getUtteranceType(self):
        return self.utteranceType
        
    def setEmotion(self,value):
        self.emotion = value
        
    def getSender(self):
        return self.sender
    
    def getReceiver(self):
        return self.receiver
    
    def getID(self):
        return self.id
    
    def getEmotion(self):
        return self.emotion
    
    def setSender(self,sender):
        self.sender = sender
        
    def setReceiver(self,receiver):
        self.receiver = receiver
        
    def setUtteranceType(self,value):
        self.utteranceType = value