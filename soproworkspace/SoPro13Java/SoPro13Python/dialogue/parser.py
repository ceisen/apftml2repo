################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 08.03.2014

@author: Christian Willms
'''
'''Parser to extract Utterances from a text'''
from UtFactory import UtteranceFactory
from nltk.corpus import wordnet as wn
import json
import nltk
import os 

class UtteranceParser:
    
    
    def __init__(self):
        '''Constructor:
        SEgrammar: Grammar used by extractSender to extract chunks
        REgrammar: Grammar used by extractReceiver to extract chunks 
        speakVerbs: Set of verbs which trigger a direct speech
        utteranceFactory: Instance of UtteranceFactory
        alreadySeen: Set of verbs already checked by checkForSpeakVerb
        '''
        self.SEgrammar = r"""
            NP:       {<DT><NN><POS><NN>}         
                      {<DT|PR.*>?<RB|JJ.*>*<NN>}       # chunk det/pos, adj and nouns
                      {<NNP>+}                         # chunk sequences of proper noun
                      {<PRP.*>}                        # pronouns
            LNP:      {<NP><JJ><IN><NP>}               # long or combined NP
            VPN:      {<VBD><NP><VBN><IN>?<NP|LNP>?}   # verbal phrase with past participle
            VPG:      {<VBD><VBG>}                     # verbal phrase with present participle
            VP:       {<VBN><RP>?}                     # verbal phrase
                      {<VBD><RP>?}                     
            THAT:     {<IN><NP><VPG>}                  
            Receiver: {<RP>?<TO|IN> <NP>}              
            VPR:      {<VP><RB><Receiver>}             # directional verbal phrase
            Con:      {<CC> <VP>}                      # conjunction followed by a verbal phrase 
            Mod:      {<IN><JJ><IN><NP><MD>}           # a sentence modifier
            Sender:   {<NP><VPR><Receiver|THAT|RB|NP|Mod|IN|,>*<Con>?}        # subject is followed by directional verbal phrase and an optional rest
                      {<NP><RB>? <VP|VPG|VPN> <Receiver|THAT|RB|NP|Mod|IN|,>*<Con>?} # the standard sentence structure in a tale 
                      {<VP><THAT>? <NP>}    # sentence build of VP and NP with or without an 
        """
        self.REgrammar = r"""
            NP:       {<DT|P.*>?<JJ>*<NN>}    # chunk det/pos, adj and nouns
                      {<NNP>+}                # chunk sequences of proper noun
                      {<PRP.*>}               # pronouns
            VP:       {<VBN><RP>}             # verbal phrase
                      {<VBD>}
            Receiver: {<VP> <TO> <NP>}        
                      {<ReceiverI>}
            ReceiverI:{<JJ><JJ>}              # expressions like "old water-splasher"
        """
        self.speakVerbsdict = {}
        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__,'speakverbs.json'),'a+') as f:
            if os.stat(os.path.join(__location__,'speakverbs.json')).st_size != 0:
                self.speakVerbsdict = json.load(f)
        self.utteranceFactory = UtteranceFactory()


    def parse(self,time):
        '''This method uses nltk to tokenize the text of a given time-object, 
        then it checks if there is any direct speech in this text, 
        by comparing delimiters.
        If there is any dialogue the method uses  utteranceFactory to create
        a new instance of Utterance and adds this instance to the result.
        If not a Utterance with sender = "narrator" will be created 
        return :[n1,...,nn]  if there is no dialogue in the span of the given 
                            time-object, with n is an "narrator"-utterance
                [u1,...,un] if there is at least one utterance in the text of 
                            the time-object, with u is a "narrator"-Utterance 
                            or a standard utterance  
        ''' 
        resultlist = []
        text = time.getSpan()
        
        if '"' not in text:
            #There is no direct speech in the span of the time object
            if text.count(".") > 1:
                sentences = nltk.sent_tokenize(text)
                for s in sentences:
                    resultlist.append(\
                            self.utteranceFactory.createNarratorUt2(time, s))
                return resultlist
            else:
                return [self.utteranceFactory.createNarratorUt(time)]
        
        text = text.replace("."," .")
        tokens =nltk.word_tokenize(text)
        delimiter = (list)([i for i, j in enumerate(tokens) if j == '"'])
        if len(delimiter) %2 != 0:
            # the number of '"' in the span isn't equal so there can't be a direct speech
            return [self.utteranceFactory.createNarratorUt(time)]
        delimiter.reverse()
        lastindex = 0   #index of the last token in a direct speech
        
        while len(delimiter) != 0:
            '''creates the utterances by separating the span of the time object 
            in direct speech and parts said by the narrator''' 
            index1 = delimiter.pop()
            index2 = delimiter.pop()
            if index2 - index1 == 2  and tokens[index1+1].islower():
                return [self.utteranceFactory.createNarratorUt(time)]
            text = ""
            for i in range(lastindex, index1):
                text += tokens[i] + " "
            if text != "":
                resultlist.append(\
                        self.utteranceFactory.createNarratorUt2(time, self.textModifier(text)))
            lastindex = index2+1
                
            text = self.extractSpan(index1, index2,tokens)
            #print text
            sender = self.extractSender(tokens,text)
            receiver = self.extractReceiver(tokens,text)
            if text.count(".") > 1:
                sentences = nltk.sent_tokenize(text)
                for s in sentences:
                    resultlist.append(self.utteranceFactory.createUt(time, s, sender,receiver))
                return resultlist
            resultlist.append(self.utteranceFactory.createUt(time,text,sender,receiver))
        
        if lastindex != len(tokens)-1:
            '''if there are more then 1 sentence in a single direct speech, 
            create an utterace for every sentence'''
            text = ""
            for i in range(lastindex, len(tokens)):
                text += tokens[i] + " "
            if text != "":
                resultlist.append(self.utteranceFactory.createNarratorUt2(time, text)) 
        return resultlist
        
      
    def extractSpan(self,index1,index2,tokens):
        ''' This method is used to extract the span of an Utterance.
        index1: representing the index of the first "-Token 
        index2: representing the index of the second "-Token
        tokens: list of all tokens
        return: a string representing the span of an Utterance, without '"' 
        '''   
        text = " ".join(tokens[index1+1:index2])
        modtext = self.textModifier(text)
        return modtext
    
    def textModifier(self,text):
        modtext = text.replace(" ,",",")
        modtext = modtext.replace(" !","!")               
        modtext = modtext.replace(" ?","?")
        modtext = modtext.replace(" .",".")
        modtext = modtext.replace(" '", "'")
        modtext = modtext.replace(" n't","n't")
        modtext = modtext.strip()
        return modtext
    
  
    def extractSender(self,tokens,text):
        '''This method is used to extract the sender of an Utterance 
        by tagging and chunking the span 
        '''

        pos_tags = nltk.pos_tag(tokens)
        #check if the text contains any speakVerbs
        self.checkVerbs(pos_tags)
        cp = nltk.RegexpParser(self.SEgrammar, loop = 1) 
        result = cp.parse(pos_tags)
        for subtree in result.subtrees():
            if subtree.node == 'Sender':
                flag = (list)(subtree.subtrees(lambda t: self.filter(t.leaves())))
                if len(flag) != 0:
                    for subsubtree in subtree.subtrees():
                        if subsubtree.node == 'NP' and subsubtree.leaves()[0][0] != '"':
                            if len(subsubtree.leaves()) > 1:
                                sender = ""
                                for i in range(len(subsubtree.leaves())):
                                    sender += subsubtree.leaves()[i][0] + " "
                                    sender = sender.replace(" 's", "'s")
                                if sender not in text:
                                    return sender.strip()
                            else:
                               
                                sender = subsubtree.leaves()[0][0] 
                                if " "+sender+" " not in text:
                                    return sender
     
        return "default"
    
     
    def extractReceiver(self,tokens,text):
        ''' This method is used to extract the receiver of an Utterance 
        by pos-tagging and chunking the span 
        ''' 
        
        pos_tags = nltk.pos_tag(tokens)
        cp = nltk.RegexpParser(self.REgrammar, loop = 1)
        result = cp.parse(pos_tags)
        
        for subtree in result.subtrees():
            if subtree.node == 'Receiver':
                flag = (list)(subtree.subtrees(lambda t: self.filter(t.leaves())))
                if len(flag) != 0:
                    #There is a speakVerb in the text
                    for subsubtree in subtree.subtrees():
                        if subsubtree.node == 'NP':
                            if len(subsubtree.leaves()) > 1:
                                receiver = ""
                                for i in range(len(subsubtree.leaves())):
                                    receiver += subsubtree.leaves()[i][0] + " "
                                if receiver not in text:
                                    return receiver.strip()
                            else:
                                receiver = subsubtree.leaves()[0][0] 
                                if " "+receiver+" " not in text:
                                    return subsubtree.leaves()[0][0]
            else:
                #There is no speakVerb in the text
                for subsubtree in subtree.subtrees():
                    if subsubtree.node == "ReceiverI":
                        receiver = ""
                        for i in range(len(subsubtree.leaves())):
                            receiver += subsubtree.leaves()[i][0] + " "
                        return receiver.strip()            
        return "receiver"
    
  
    def filter(self,leaves):
        ''' This is a helper, which checks if a given list of tuples 
        contains speakverbs. 
        ''' 
        for i in leaves:
            if i[0] in (set)(self.speakVerbsdict.keys()):
                return self.speakVerbsdict.get(i[0])          
        return False
    
    
    def checkForSpeakVerb(self,verb):
        '''uses the similarity of an verb to several Synsets to calculate 
        if the given verb is a speakVerb
        ''' 
        simAnswer= self.getMaxSim(verb, wn.synset('answer.v.01'))   #@UndefinedVariable
        simThink= self.getMaxSim(verb, wn.synset('think.v.11'))     #@UndefinedVariable
        simSay= self.getMaxSim(verb, wn.synset('allege.v.01')) * 1.5    #@UndefinedVariable
        simAsk= self.getMaxSim(verb, wn.synset('ask.v.05'))         #@UndefinedVariable
        simCry = self.getMaxSim(verb, wn.synset('shout.v.02'))      #@UndefinedVariable
        simGive = self.getMaxSim(verb, wn.synset('give.v.01'))      #@UndefinedVariable
        prob = simAnswer + simThink + simSay + simAsk + simCry 
        if prob >= 10 and simGive < 3:
            return True
        else:
            return False
        
        
        
    def getMaxSim(self,word,synset):
        '''uses nltk.wordnet to compute the maximal similarity between a word 
        and a given Synset.
        This method is used by checkForSpeakVerb.'''
        maxSim = 0
        for syn in wn.synsets(word, wn.VERB): #@UndefinedVariable
            sim =  syn.lch_similarity(synset)
            if sim >= maxSim:
                maxSim = sim
        return maxSim


    def checkVerbs(self, pos_tags):
        '''This method checks if there is any speakVerb in the tagged text and 
        adds such verbs to self.speakVerbs. Also adds every checked Verb to 
        seld.alreadySeen
        '''
        for tag in pos_tags:
            if "VB" in tag[1]:
                #changed
                if tag[0] not in self.speakVerbsdict.keys():
                    #self.alreadySeen.add(tag[0])
                    if self.checkForSpeakVerb(tag[0]):
                        #self.speakVerbs.add(tag)
                        self.speakVerbsdict[tag[0]] = True
                    else:
                        self.speakVerbsdict[tag[0]] = False


                    