################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 08.03.2014

@author: Christian Willms
'''
'''The DialogueExtractor parses time-Object to extract utterances 
and combines these utterances to dialogues'''

from parser import UtteranceParser
from dialogueRep import Dialogue
import textmining as tm
import json
import os


class DialogueExtractor:
   
    
    def __init__(self):
        '''constructor:
        tale: name of the parsed tale
        utterances : list of every utterance 
        parser : instance of UtteranceParser
        coherentUt: List of Lists with coherent utterances
        dialogues: list of dialogues
        timeToUtterances: dictionary matches timeId -> list of Utterances
        names: Set of names in this tale
        timeline: the timeline of the parsed tale
        ''' 
        self.tale = ""
        self.parser = UtteranceParser()
        self.coherentUT = []
        self.dialogues = []
        self.timeToUtterances = {}
        self.names = set()
        self.timeline = []
        self.utterances = []
        self.__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
       
        
    
    def parseTime(self,time):
        '''parses a given time and fills the timeToUtterance dictionary
         and the utterances list. 
         Further this method checks if the given time contains a name, using 
         self.checkName 
         '''
        self.checkName(time.getSpan())
        utterances = self.parser.parse(time)
        self.timeToUtterances[time.getId()] = utterances
        self.utterances.extend(utterances)
            
    
    def combineUtToDialogue(self):
        '''combines coherent utterances to a dialogue for every element in 
        self.coherentUt
        '''
        dialogues = []
        for utterances in self.coherentUT:
            dialogue = Dialogue(utterances,self.names,self.utterances)
            dialogues.append(dialogue)
        return dialogues
                

        
    def computeCUT(self,timeline):
        ''' computes lists of coherent utterances using several rules and 
        adds those to self.coherentUt  
        '''
        coherent = []
        narratorFlag = False
        for t in timeline:
            #iterates on the timeToUtterance dict using the timeline as order
            timeId = t.getId()
            utterances = self.timeToUtterances.get(timeId)
            if len(coherent) == 0:
                coherent.extend(utterances)
                if utterances[0].getSender() == "narrator" \
                and len(utterances) == 1:
                    narratorFlag = True
            else:
                if narratorFlag:
                    if utterances[0].getSender() == "narrator" \
                    and len(utterances) == 1:
                        coherent.extend(utterances)
                    else:
                        self.coherentUT.append(coherent)
                        coherent = []
                        coherent.extend(utterances)
                        narratorFlag = False
                else :
                    if utterances[0].getSender() == "narrator" \
                    and len(utterances) == 1:
                        self.coherentUT.append(coherent)
                        coherent = []
                        coherent.extend(utterances)
                        narratorFlag = True
                    else:
                        coherent.extend(utterances)
        self.coherentUT.append(coherent)
            
            
    def parseTimeLine(self,timeline):
        '''iterates on a given list of time objects and parses every element.
        thereafter the method computes the coherent utterances and 
        build the dialogues out of this coherent utterances.
        ''' 
        self.timeline = timeline
        print "\t| parsing timeline"
        for time in timeline:
            self.parseTime(time)
        print "\t| computing coherent Utterances"
        self.computeCUT(timeline)
        print "\t| combining utterances to dialogues"
        self.dialogues = self.combineUtToDialogue()
        for dia in self.dialogues:
            dia.addInformationsToDialogue()  
        with open(os.path.join(self.__location__, 'speakverbs.json'), 'w',) as f:
            (json.dump(self.parser.speakVerbsdict,f))
        
            
    def checkName(self,text):
        ''' Check if a given text contains any names
        '''
        tokens = text.split()
        for token in tokens:
            if token.lower() in tm.names_male and token[0].isupper(): 
                self.names.add(token)
            if token.upper() in tm.names_last and token[0].isupper():
                self.names.add(token)
        
                        
### Getter and Setter ###              
    def setTale(self,name):
        self.tale = name
        
    def setUtterances(self,utlist):
        self.utterances = utlist      
    
    def getUtterances(self):
        return self.utterances
        
    def setcoherentUt(self,utlist):
        self.coherentUT = utlist
        
    def getcoherentUT(self):
        return self.coherentUT
    
    def getTimeForID(self,identifier):
        return self.times[identifier]