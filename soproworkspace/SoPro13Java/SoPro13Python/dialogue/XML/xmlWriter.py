################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 07.04.2014

@author: Christian Willms
'''

from xml.etree import ElementTree as ET
from xml.dom import minidom

class Writer:

    def __init__(self,name,ID):
        '''Constructor:
        top: top element of the xml-structure
        tale: subelement of top containing the name of the tale
        '''
        self.name = name
        self.ID = ID
        self.top = ET.Element('annotated_ft'+ (str)(self.ID))
        #comment = ET.Comment('Test Comment')
        #self.top.append(comment)
        self.tale = ET.SubElement(self.top, 'tale',{'title':self.name})
        


    def prettify(self,elem):
        """Return a pretty-printed XML string for the Element.
        """
        rough_string = ET.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")


    def addTimes(self,timeList):
            '''adds the time structure to the xml
            '''
            times = ET.SubElement(self.tale, 'times')
            for t in timeList:
                time = ET.SubElement(times, "time",{'ID':str(t[0].getId()), "Span": str(t[0].getSpan())})
                relations = ET.SubElement(time, "Relations")
                for r in t[1]:
                    ET.SubElement(relations,'Relation',{"Type":str(r[0]),"TargetID":str(r[1])}) 


    def addDialogues(self,diaList):
        ''' adds the dialogues with all associated utterances as well as the 
        span of the dialogue to the xml.
        '''
        dialogues = ET.SubElement(self.tale, 'dialogues')
        for dia in diaList:
            minTime = dia.getMinTime()
            maxTime = dia.getMaxTime()
            dialogue = ET.SubElement(dialogues, "dialogue", {'ID':str(dia.getID()),'MinTime' : str(minTime),'MaxTime': str(maxTime)})
            span = ET.SubElement(dialogue,'Span')
            span.text = str(dia.getText())
            participants = ET.SubElement(dialogue,'Participants')
            for p in dia.getParticipants():
                ET.SubElement(participants,'participant',{'String':str(p)})
            utterances = ET.SubElement(dialogue, 'Utterances')
            for ut in dia.getUtterances():
                utterance = ET.SubElement(utterances,'utterance',{'ID':str(ut.getID()), 'type':str(ut.getUtteranceType()),\
                         'sender':str(ut.getSender()), 'receiver':str(ut.getReceiver()), 'time':str(ut.getTime().getId()),'emotion': str(ut.getEmotion())})
                utterance.text = str(ut.getText())
        #print self.prettify(self.top)    
        
        
    def buildXML(self,timeList,diaList):
        '''build a xml containing informations of the given timeline and 
        dialogues. 
        Further writes the xml to "pythonToOwl.xml"
        '''
        self.addTimes(timeList)
        self.addDialogues(diaList)
        self.writeXML()
        
        
    def writeXML(self):
        '''writes a prettified version of the xml-structure to pythonToOwl.xml
        '''
        with open('../annotated_ft' + (str)(self.ID) + '.xml', 'w') as f:
            f.write(self.prettify(self.top))
            