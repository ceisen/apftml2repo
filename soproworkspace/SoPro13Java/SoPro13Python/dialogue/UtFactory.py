################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 27.04.2014

@author: Christian Willms
'''
from utteranceRep import Utterance
import nltk 

class UtteranceFactory:
    
    def __init__(self):
        '''
        Constructor:
        creates a dictionary which maps pronouns to the number their appearance
        in the tale.
        '''
        self.pronouns = {}
        self.pronouns["i"] = 0
        self.pronouns["I"] = 0
        self.pronouns["you"] = 0
        self.pronouns["You"] = 0
        self.pronouns["he"] = 0
        self.pronouns["He"] = 0
        self.pronouns["she"] = 0
        self.pronouns["She"] = 0
        self.pronouns["it"] = 0
        self.pronouns["It"] = 0
        self.pronouns["we"] = 0
        self.pronouns["We"] = 0
        self.pronouns["They"] = 0
        self.pronouns["they"] = 0
        self.pronouns["me"] = 0
        self.pronouns["Me"] = 0
        self.pronouns["him"] = 0
        self.pronouns["Him"] = 0
        self.pronouns["her"] = 0
        self.pronouns["Her"] = 0
        self.pronouns["us"] = 0
        self.pronouns["Us"] = 0
        self.pronouns["them"] = 0
        self.pronouns["Them"] = 0
        self.lastId = -1
    
    
    def createUt(self,time,utteranceText,sender,receiver):
        '''creates a standard utterance for a given time with a given sender 
        and receiver.
        ''' 
        if time.getId() != self.lastId:
            self.increaseCounter(time.getSpan())
            self.lastId = time.getId()
        return Utterance(time,utteranceText,self.addCounter(sender),\
                         self.addCounter(receiver),self.pronouns)
    
    
    def createNarratorUt(self,time):
        '''creates an utterance with sender = "narratr" and receiver = "reader"
        for a given time.
        The text of the utterance is the entire span of this time.
        '''
        ut = Utterance(time,time.getSpan(),"narrator", "reader",self.pronouns)
        ut.setUtteranceType("inform")
        if time.getId() != self.lastId or time.getId() == 0:
            self.increaseCounter(time.getSpan())
            self.lastId = time.getId()
        return ut
    
    
    def createNarratorUt2(self,time,text):
        '''creates an utterance with sender = "narratr" and receiver = "reader"
        for a given time.The text of the utterance is the given text.
        '''
        ut = Utterance(time,text,"narrator", "reader", self.pronouns)
        ut.setUtteranceType("inform")
        if time.getId() != self.lastId or time.getId() == 0:
            self.increaseCounter(time.getSpan())
            self.lastId = time.getId()
        return ut
    
    
    def addCounter(self,text):
        '''adds the counter to the text, if this one is a pronoun.
        '''
        if text not in self.pronouns.keys():
            return text
        else:
            text += "_"+ str(self.pronouns.get(text))
            return text
    
    
    def increaseCounter(self,text):
        ''' Increases the counter of every pronoun in self.pronouns, 
        if these occur in the text.
        ''' 
        tokens =nltk.word_tokenize(text)
        for p in self.pronouns.keys():
            if p in tokens:
                self.pronouns[p] +=1
