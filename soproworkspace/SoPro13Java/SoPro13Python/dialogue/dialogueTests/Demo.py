################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 20.05.2014

@author: Christian Willms
'''
import sys
sys.path.insert(0, './lib/nltk-2.0.1rc1-py2.6.egg')
sys.path.insert(0, './time')
sys.path.append('./emotion')
sys.path.append('./dialogue')
sys.path.append('./dialogue/XML')
sys.path.append('./lib')

from timeParser import TimeParser
from dialogue.dialogueExtractor import DialogueExtractor
from XML.xmlWriter import Writer
from emotion import EmotionFactory as em 
    
if __name__ == "__main__":
    tparser = TimeParser()
    print "building time structure..."
    taleTxt = sys.argv[1]
    taleID = sys.argv[2]
    tstructure = tparser.parse(taleTxt)
    talename = tstructure.taleName.replace(".txt", "")
    XMLTimeLine = []
    ParsingTimeLine = []
    for i in tstructure.structure:
        XMLTimeLine.append(tstructure.structure[i])
        ParsingTimeLine.append(tstructure.structure[i][0])
    print "...finished"
    print "extracting dialogues..."
    extractor = DialogueExtractor()
    extractor.parseTimeLine(ParsingTimeLine)
    dialogues = extractor.dialogues
    print "...finished"
    print "recognizing emotions..."
    emFactory = em.EmoFactory(dialogues)
    emFactory.setEmo()
    print "...finished"
    print "building and writing xml..."
    myWriter = Writer(talename, taleID)
    myWriter.buildXML(XMLTimeLine, dialogues)
    print "...finished"