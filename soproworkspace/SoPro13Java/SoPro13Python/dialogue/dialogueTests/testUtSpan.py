################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 05.03.2014

@author: Christian Willms
'''
import timeObjects as t
from dialogue.parser import UtteranceParser
import unittest

'''
This class is used to test if the span of an Utterance is correctly extracted'''
class TestSpan(unittest.TestCase):
        
       
    '''In the given time-object is no Dialogue/ no Utterance'''
    def test_NoDialogue(self):
        myParser = UtteranceParser()
        time1 = t.timeObject(1,"Once there was a miller who was poor, but who had a beautiful daughter.",None)
        time5 = t.timeObject(5,"Thereupon he himself locked up the room, and left her in it alone.",None)
        result = myParser.parse(time1)
        self.assertEqual(len(result),1,  "There is a 'Narrator'- Utterance at this time but none was found")
        self.assertEqual(result[0].getText(), "Once there was a miller who was poor, but who had a beautiful daughter."  , "wrong span, was " + result[0].getText())
        result = myParser.parse(time5)
        self.assertEqual(len(result),1,  "There is a 'Narrator'- Utterance at this time but none was found")
        self.assertEqual(result[0].getText(),"Thereupon he himself locked up the room, and left her in it alone." , "wrong span, was "+ result[0].getText())
        
        
    '''In the given time-object is only one Dialogue/ Utterance '''
    def test_OneUtterance(self):
        myParser = UtteranceParser()
        time2 = t.timeObject(2,"Now it happened that he had to go and speak to the King, and in order to make himself appear important he said to him, \" I have a daughter who can spin straw into gold.\"",None)
        time4 = t.timeObject(4,"And when the girl was brought to him he took her into a room which was quite full of straw, gave her a spinning-wheel and a reel, and said, \"Now set to work, and if by tomorrow morning early you have not spun this straw into gold during the night, you must die.\"",None)
        time3 = t.timeObject(2,"The King said to the miller, \"That is an art which pleases me well, if your daughter is as clever as you say, bring her tomorrow to my palace, and I will try what she can do.\"",None)
        result= myParser.parse(time2)
        self.assertEqual(len(result),2, "There are 2 Utterances at this but found " + str(len(result)))
        self.assertEqual(result[0].getText(),"Now it happened that he had to go and speak to the King, and in order to make himself appear important he said to him," , "wrong span, was " + "--"+result[0].getText()+"--")
        self.assertEqual(result[1].getText(),"I have a daughter who can spin straw into gold." , "wrong span, was " + "--"+result[1].getText()+"--")
        result= myParser.parse(time3)
        self.assertEqual(len(result),2, "There are 2 Utterances at this but found " + str(len(result)))
        self.assertEqual(result[1].getText(),"That is an art which pleases me well, if your daughter is as clever as you say, bring her tomorrow to my palace, and I will try what she can do." , "wrong span")
        result= myParser.parse(time4)
        self.assertEqual(len(result),2, "There are 2 Utterances at this but found " + str(len(result)))
        self.assertEqual(result[1].getText(),"Now set to work, and if by tomorrow morning early you have not spun this straw into gold during the night, you must die." , "wrong span")
        
    '''In the given time-object are multiple Utterances'''
    def test_MultiUtterances(self):
        myParser = UtteranceParser()
        time6 = t.timeObject(6,"\"Alas!\" answered the girl, \"I have to spin straw into gold, and I do not know how to do it.\"",None)
        result = myParser.parse(time6)
        self.assertEqual(len(result),3,"There are 3 Utterance at this but found " + str(len(result)))
        self.assertEqual(result[0].getText(), "Alas!", "Wrong span1, was " +"--"+   result[0].getText()+ "--")
        self.assertEqual(result[1].getText(), "answered the girl,", "Wrong span2, was " +"--"+   result[1].getText()+ "--")
        self.assertEqual(result[2].getText(), "I have to spin straw into gold, and I do not know how to do it.", "Wrong Span3, was " +"--"+   result[2].getText()+ "--")
    
if __name__ == '__main__':
    unittest.main()
