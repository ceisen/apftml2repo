################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 14.05.2014

@author: Christian Willms
'''
import sys
sys.path.insert(0, './lib/nltk-2.0.1rc1-py2.6.egg')
sys.path.insert(0, './time')
sys.path.append('./emotion')
sys.path.append('./dialogue')
sys.path.append('./dialogue/XML')
sys.path.append('./lib')

from timeParser import TimeParser
from dialogue.dialogueExtractor import DialogueExtractor
from XML.xmlWriter import Writer
from emotion import EmotionFactory as em 
import unittest
import os

'''
This class is used to test if the dialogues in "Frog-King" are correctly computed '''
class Demo(unittest.TestCase):
    
    
    def test_Frog_King(self):
        tparser = TimeParser()
        # filenames: The-Swan-Geese.txt, The Bremen Town Musicians.txt, The-Frog-King.txt, Rumpelstiltskin.txt
        print "build time structure..."
        tstructure = tparser.parse('The-Frog-King.txt')
        talename = tstructure.taleName.replace(".txt", "")
        taleID = 1
        XMLTimeLine = []
        ParsingTimeLine = []
        for i in tstructure.structure:
            XMLTimeLine.append(tstructure.structure[i])
            ParsingTimeLine.append(tstructure.structure[i][0])
            # print tstructure.structure[i]
        print "...finished"
        print "extract dialogues..."
        extractor = DialogueExtractor()
        extractor.parseTimeLine(ParsingTimeLine)
        dialogues = extractor.dialogues
        print "...finished"
        print "start emotion extraction..."
        emFactory = em.EmoFactory(dialogues)
        emFactory.setEmo()
        print "...finished"
        print 'build xml and write to "pythonToOwl"...'
        myWriter = Writer(talename,taleID)
        myWriter.buildXML(XMLTimeLine, dialogues)
        print "...finished"

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
        
