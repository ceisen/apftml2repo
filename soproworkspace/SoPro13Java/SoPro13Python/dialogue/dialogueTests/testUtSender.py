################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 10.03.2014

@author: Christian Willms
'''
import timeObjects as t
from dialogue.parser import UtteranceParser
import unittest

'''
This class is used to test if the sender of an Utterance is correctly extracted'''
class TestSender(unittest.TestCase):
    
    
    def test_noSenderinTime(self):
        """There is no clearly recognizable Sender in the Time-Object""" 
        myParser = UtteranceParser()
        time1 = t.timeObject(1,"\"Perhaps your name is Rumpelstiltskin?\"",None)
        time2 = t.timeObject(2,"\"No!\"",None)
        result= myParser.parse(time1)
        self.assertEqual(result[0].getSender(), "default", "There was no Sender")
        result= myParser.parse(time2)
        self.assertEqual(result[0].getSender(), "default", "There was no Sender")
        
    
    def test_OnePosSender(self):
        """There is only clearly recognizable Sender in the Time-Object. The syntactical structure 
        of the Span is very simple.""" 
        myParser = UtteranceParser()
        time1 = t.timeObject(1,"\"The devil has told you that! the devil has told you that!\" cried the little man.",None)
        time2 = t.timeObject(2,"At first she said, \"Is your name Conrad?\"",None)
        time3 = t.timeObject(3,"\"Be still and stop crying,\" answered the frog.",None)
        time4 = t.timeObject(4,"The frog answered, \"I do not want your clothes, your pearls and precious stones, nor your golden crown, but if you will love me and accept me as a companion and playmate, and let me sit next to you at your table and eat from your golden plate and drink from your cup and sleep in your bed, if you will promise this to me, then I'll dive down and bring your golden ball back to you.\"",None)
        time5 = t.timeObject(5," \"Oh, it's you, old water-splasher,\" she said. \"I am crying because my golden ball has fallen into the well.\"",None)
        time17 = t.timeObject(17, "\"Wait, wait,\" called the frog, \"take me along. I cannot run as fast as you.\"",None)
        result = myParser.parse(time1)
        self.assertEqual(result[0].getSender(), "the little man" , "Wrong sender, expected: the litte man, but was " +result[0].getSender())
        #self.assertEqual(result[1].getSender(), "narrator" , "Wrong sender, expected: narrator, but was " +result[1].getSender())
        result = myParser.parse(time2)
        self.assertEqual(result[0].getSender(), "narrator", "Wrong sender, expected: narrator, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "she_1" , "Wrong sender, expected: the litte man, but was " +result[1].getSender())
        result = myParser.parse(time3)
        self.assertEqual(result[0].getSender(), "the frog", "Wrong sender, expected: the frog, but was " +result[0].getSender())
        #self.assertEqual(result[1].getSender(), "narrator" , "Wrong sender, expected: the litte man, but was " +result[1].getSender())
        result = myParser.parse(time4)
        self.assertEqual(result[0].getSender(), "narrator", "Wrong sender, expected: the frog, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "The frog" , "Wrong sender, expected: the litte man, but was " +result[1].getSender())
        result = myParser.parse(time5)
        self.assertEqual(result[0].getSender(), "she_2", "Wrong sender, expected: she_2, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "narrator" , "Wrong sender, expected: the litte man, but was " +result[1].getSender())
        result = myParser.parse(time17)
        self.assertEqual(result[0].getSender(), "the frog", "Wrong sender, expected: the frog, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "narrator" , "Wrong sender, expected: the litte man, but was " +result[1].getSender())
    
    def test_multiPosSender(self):
        """There are 2 or more possible Sender in the Time-Object. """ 
        myParser = UtteranceParser()
        time1 = t.timeObject(1,"Now it happened that he had to go and speak to the King, and in order to make himself appear important he said to him, \" I have a daughter who can spin straw into gold.\"",None)
        result = myParser.parse(time1)
        self.assertEqual(result[0].getSender(), "narrator", "Wrong Sender, expected: he, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "he_1", "Wrong Sender, expected: he, but was " +result[0].getSender())
    
    def test_complexSender(self):
        """There is only clearly recognizable Sender in the Time-Object. The syntactical structure 
        of the Span is complex."""     
        myParser = UtteranceParser()
        time1 = t.timeObject(1," He sat there and called out, \"Lift me up next to you.\"",None)
        time2 = t.timeObject(2,"The king saw that her heart was pounding and asked, \"My child, why are you afraid? Is there a giant outside the door who wants to get you?\"",None)
        time3 = t.timeObject(3,"The king became angry and said, \"You should not despise someone who has helped you in time of need.\"",None)
        time4 = t.timeObject(4,"As she was lying in bed, he came creeping up to her and said, \"I am tired, and I want to sleep as well as you do. Pick me up or I'll tell your father.\"",None)
        time5 = t.timeObject(5,"He turned around and said, \"Heinrich, the carriage is breaking apart\"",None)
        result = myParser.parse(time1)
        self.assertEqual(result[0].getSender(), "narrator","Wrong Sender, expected: narrator, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "He_1","Wrong Sender, expected: He, but was " +result[1].getSender())   
        result = myParser.parse(time2)
        self.assertEqual(result[0].getSender(), "narrator","Wrong Sender, expected: narrator, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "The king","Wrong Sender, expected: The king, but was " +result[1].getSender())
        result = myParser.parse(time3)
        self.assertEqual(result[0].getSender(), "narrator","Wrong Sender, expected: narrator, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "The king","Wrong Sender, expected: The king, but was " +result[1].getSender())   
        result = myParser.parse(time4)
        self.assertEqual(result[0].getSender(), "narrator","Wrong Sender, expected: narrator, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "he_1","Wrong Sender, expected: he, but was " +result[1].getSender())
        result = myParser.parse(time5)
        self.assertEqual(result[0].getSender(), "narrator","Wrong Sender, expected: narrator, but was " +result[0].getSender())
        self.assertEqual(result[1].getSender(), "He_2","Wrong Sender, expected: He_2, but was " +result[1].getSender())