################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################ 

'''
Created on 08.03.2014

@author: Christian Willms
'''
import nltk
import pattern.text.en as p
#from utterance import Utterance

'''This class represents a dialogue ''' 
class Dialogue:
    
    DialogueID = 0
    
    
    def __init__(self,utterances,names,allUtterances):
        '''constructor:
        utterances : the utterances of this dialogue
        names : a List of possible Names 
        allUtterances: a list of all utterances in the tale 
        '''
        self.id = Dialogue.DialogueID
        self.utterances = utterances
        self.times = {}
        self.timeIds = self.extractTimeIDs()
        self.participants = set()
        #self.fillParticipants()
        self.names = names
        self.allUtterances = allUtterances
        Dialogue.DialogueID += 1
        
    
    
    def fillParticipants(self):
        '''adds the receiver and sender of every utterance in this dialogue to 
        self.participants 
        '''
        for ut in self.utterances:
            sender = ut.getSender()
            receiver = ut.getReceiver()
            if sender !="default":
                self.participants.add(sender)
            if receiver !="receiver":
                self.participants.add(receiver)
            
            
    def extractTimeIDs(self):
        '''returns a list of all TimeID's in this dialogue.
        used by __init__  
        '''
        result = []
        for ut in self.utterances:
            identifier = ut.getTime().getId()
            if identifier not in result:
                result.append(identifier)
                self.times[identifier] = ut.getTime().getSpan()
        return result
    
    
    def extractSubject(self,utterance):
        s = utterance.getText()
        s = p.parsetree(s,relations=True,lemmata=True) 
        sub = ""
        while sub == "":
            for sentence in s:
                ###new
                if sentence.subjects != []:
                    sub = sentence.subjects[0].string 
                    return sub
            sub ="default" 
        for sentence in s:
            sub =  next(chunk.string for chunk in sentence.chunks if chunk.type=="NP")
        return sub
    
    
    def extractObject(self, utterance):
        ''' This method extracts the object of an utterance .
        Used by addInformationToDialogue if there is no possibility to extract the 
        receiver of an utterance by rules. 
        '''
        receiver = "receiver"
        text = utterance.getText()
        if "to" in text:
            tokens = nltk.word_tokenize(utterance.getText())
            pos = nltk.pos_tag(tokens)
            if ("to","TO") in pos:
                posObject = pos[pos.index(("to","TO"))+1]
                if posObject[1] == "PRP$":
                    receiver = posObject[0]
                    return receiver
        return receiver
    
    
    def editUtteranceType(self, utBefor, ut):
        '''Uses several rule to extract and edit the type of an utterance 
        according to the previous utterance 
        '''
        if ut.getUtteranceType() == "inform":
            if utBefor.getUtteranceType() == "question":
                ut.setUtteranceType("answer")
            if utBefor.getUtteranceType() == "answer":
                ut.setUtteranceType("answer")
            if utBefor.getTime().getId() == ut.getTime().getId():
                #print "cond",ut.getID(),": ", utBefor.getID()
                ut.setUtteranceType(utBefor.getUtteranceType())
    
    
    def editSender(self, utBefor, ut):
        '''Uses several rule to extract and edit the sender of an utterance 
        according to the previous utterance 
        '''
        if ut.getSender() == "default":
            if "Oh" in utBefor.getText() or ut.getText().startswith("But"):
                ut.setSender(utBefor.getSender())
            elif ut.getTime().getId() == utBefor.getTime().getId() and ut.getID() != utBefor.getID():
                ut.setSender(utBefor.getSender())
            elif ut.getUtteranceType() == "answer":
                if "I" and "?" in utBefor.getText() and utBefor.getUtteranceType() == "answer" :
                    ut.setSender(utBefor.getSender())
                else:
                    ut.setSender(utBefor.getReceiver())
            elif ut.getUtteranceType() == "question":
                if utBefor.getUtteranceType()=="answer" and len(nltk.word_tokenize(utBefor.getText())) == 2:
                    ut.setSender(utBefor.getReceiver())
                elif utBefor.getReceiver() == "receiver"or len(nltk.word_tokenize( utBefor.getText())) <=5:
                    ut.setSender(utBefor.getSender())
                else:
                    ut.setSender(utBefor.getReceiver())
            elif ut.getUtteranceType() == "inform" and utBefor.getReceiver() != "receiver" :
                if self.times[ut.getTime().getId()].startswith("And"):
                    ut.setSender(utBefor.getSender())
                else:
                    ut.setSender(utBefor.getReceiver())
                    ut.setReceiver(utBefor.getSender())
                self.participants.add(utBefor.getReceiver())
            else:
                lastUtterance = self.allUtterances[ut.getID() - 1]
                sender = self.extractSubject(lastUtterance)
                if sender in lastUtterance.pronouns:
                    sender = sender + "_" + str(lastUtterance.pronouns.get(sender))
                ut.setSender(sender)
    
    
    def editReciever(self,utBefor, ut):
        '''Uses several rule to extract and edit the receiver of an utterance 
        according to the previous utterance 
        '''
        if ut.getReceiver() == "receiver":
            if utBefor.getSender() == ut.getSender() \
                and ut.getID() != utBefor.getID():
                ut.setReceiver( utBefor.getReceiver())
            elif ut.getUtteranceType() == "monologue":
                ut.setReceiver(ut.getSender())
            
            elif ut.getUtteranceType() == "answer":
                if ut.getTime() != utBefor.getTime():
                    ut.setReceiver(utBefor.getSender())
                   
                else:
                    ut.setReceiver(utBefor.getReceiver())
                   
            elif ut.getUtteranceType() == "question" and \
                 utBefor.getSender() != ut.getSender():
                ut.setReceiver(utBefor.getSender())
                
                  
            elif ut.getUtteranceType() == "inform" \
                and utBefor.getSender() != "default" \
                and utBefor.getSender() != ut.getSender():
                ut.setReceiver(utBefor.getSender())
                
            else:   
                tokens = nltk.word_tokenize(ut.getText())
                tags = nltk.pos_tag(tokens)
                if tags[0][1] == 'NN' and tags[1][1] == ',':
                    receiver = tags[0][0]
                    ut.setReceiver(receiver)
                    
                if tags[0][1] == 'PRP$'and tags[1][1]== 'NN' \
                                       and tags[2][1]== ',':
                    receiver = tags[0][0]+ " " + tags[1][0]
                    ut.setReceiver(receiver)
                   
            # Check if a name occurs in the utterance 
            for name in self.names:
                if name in ut.getText() and name != ut.getSender():
                    ut.setReceiver(name)
                           
            if ut.getReceiver() == "receiver":
                lastUtterance = self.allUtterances[ut.getID()-1]
                receiver = self.extractObject(lastUtterance)
                if receiver in lastUtterance.pronouns:
                    receiver = receiver + "_" + str(lastUtterance.pronouns.get(receiver))
                ut.setReceiver(receiver)
               
    
    
    def addInformationsToDialogue(self):
        ''' adds further informations (sender, receiver and utteranceType) 
        to the utterances of the dialogue using editUtteranceType, 
        editSender and editReceiver.
         
        '''
        utBefor  = self.getFirstNonNarratorUT()
        if utBefor != None:  
            for ut in self.utterances:
                #print ut.getID(), ut.getText()
                if ut.getSender() != "narrator":
                    
                    #print "Add Information to type"
                    self.editUtteranceType(utBefor, ut)
                            
                    #print"Add Information to sender"
                    self.editSender(utBefor, ut)
                            
                    #print"Add Information to receiver" 
                    self.editReciever(utBefor, ut)
                            
                if ut.getSender() != "narrator" :
                    utBefor = ut
        self.fillParticipants()
    
    def getFirstNonNarratorUT(self):
        '''returns the first Utterances with sender != "narrator"
        called by addInformationsToDialogue 
        '''
        for i in range(len(self.utterances)):
            ut = self.utterances[i]
            if ut.getSender() != "narrator":
                return ut
        return None
    
    
### Getter and Setter ### 
    
    def getID(self):
        return self.id
    
    def getParticipants(self):
        return self.participants
    
    def getText(self):
        text = ''
        for identifier  in self.timeIds:
            text += self.times.get(identifier) + " "
        return text
    
    def getUtterances(self):
        return self.utterances
    
    def getTimeIds(self):
        return self.timeIds
    
    def getMinTime(self):
        return self.timeIds[0]
    
    def getMaxTime(self):
        return self.timeIds[len(self.timeIds)-1]