################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################
'''
Created on 02.04.2014

@author: Tonio
'''

'''
Time Object class.
Time objects are represented by their type and text span inside of printing operations.
'''

class timeObject():
    def __init__(self, identification, textSpan, category):
        self.id = identification
        self.text = textSpan
        self.type = category
        
    def __repr__(self):
        return self.type+', '+self.text
        
    def __str__(self):
        return self.type+', '+self.text
    
    def getId(self):
        return self.id
    
    def getSpan(self):
        return self.text
    