################################################################################
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
################################################################################
'''
Created on 23.04.2014

@author: Tonio
'''

"""
File used for the testing of time parser.
"""
from timeParser import TimeParser 
import os

    
parser = TimeParser()
# filenames: The-Swan-Geese.txt, The Bremen Town Musicians.txt, The-Frog-King.txt, Rumpelstiltskin.txt
structure = parser.parse('SoPro13Python'+os.sep +'data'+os.sep+ 'The Bremen Town Musicians.txt')

structure.reprStruct()