#*******************************************************************************
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
#*******************************************************************************
#!/bin/bash
echo "Welcome to the APftML2 tool."
echo "Please select a fairy tale"
select ft in The-Frog-King Rumpelstiltskin The-Bremen-Town-Musicians
do
   case "$ft" in
        The-Frog-King ) name=${ft}.txt; id=1; break;;
        Rumpelstiltskin ) name=${ft}.txt; id=2; break;;
        The-Bremen-Town-Musicians ) name=${ft}.txt; id=4; break;;
   esac
done

while true; do
    read -p "enable the TTS module? (y/n)
" yn
    case $yn in
        [Yy]* ) tts=true; break;;
        [Nn]* ) tts=false; break;;
        * ) echo "Please answer yes or no.";;
    esac
done

# Step 1: look up references
java -classpath demo.jar exec.OntologyRefLookupExecutor init.owl http://www.sopro13.de/SoPro# ./SoPro13Python/emotion/references.xml $id

# Step 2: extract time, dialogues and emotions
cd SoPro13Python
python Demo.pyc $name $id

# Step 3: populate the ontology
cd ..
java -classpath demo.jar exec.OntologyPopulatorExecutor init.owl http://www.sopro13.de/SoPro# annotated $id

if [ "$tts" = true ] ; then
    # Step 4: extract the script and play
    java -classpath demo.jar exec.StoryPlayerExecutor init.owl http://www.sopro13.de/SoPro# $id
fi
