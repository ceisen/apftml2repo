#*******************************************************************************
# Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
#*******************************************************************************
#!/bin/bash

if [ -z $1 ] || [ -z $2 ]; then
    echo "Not enough arguments provided. Usage: ./run_demo.sh <id> <filename>"
    exit 2
fi

id=$1
name=$2


# Step 1: look up references
java -classpath demo.jar exec.OntologyRefLookupExecutor init.owl http://www.sopro13.de/SoPro# ./SoPro13Python/emotion/references.xml $id
exitValue=$?
if [ $exitValue != 0 ] ; then
    echo "ERROR: Reference lookup came up empty. Check <id>."
    exit $exitValue
fi


# Step 2: extract time, dialogues and emotions
cd SoPro13Python
python Demo.pyc $name $id
exitValue=$?
if [ $exitValue != 0 ] ; then
    echo "ERROR: Fairytale text file not found. Check <filename>."
    exit $exitValue
fi


# Step 3: populate the ontology
cd ..
java -classpath demo.jar exec.OntologyPopulatorExecutor init.owl http://www.sopro13.de/SoPro# annotated $id


# Step 4: extract the script and play
java -classpath demo.jar exec.StoryPlayerExecutor init.owl http://www.sopro13.de/SoPro# $id
