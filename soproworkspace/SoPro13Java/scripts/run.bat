@rem ***************************************************************************
@rem Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
@rem ***************************************************************************
@echo off
IF [%1]==[] goto ABORT
IF [%2]==[] goto ABORT

set id=%1
set name=%2

java -classpath demo.jar exec.OntologyRefLookupExecutor init.owl http://www.sopro13.de/SoPro# ./SoPro13Python/emotion/references.xml %id%
cd SoPro13Python
python Demo.pyc %name% %id%
cd ..
java -classpath demo.jar exec.OntologyPopulatorExecutor init.owl http://www.sopro13.de/SoPro# annotated %id%
java -classpath demo.jar exec.StoryPlayerExecutor init.owl http://www.sopro13.de/SoPro# %id%
GOTO:eof

:ABORT
echo Not enough arguments provided. Usage: run_demo.bat ^<id^> ^<filename^>
