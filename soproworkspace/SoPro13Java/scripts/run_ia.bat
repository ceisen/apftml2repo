@rem ***************************************************************************
@rem Licensed under LGPL. See <docs/LICENSE.txt> or <http://opensource.org/licenses/LGPL-3.0>
@rem ***************************************************************************
@echo off
echo "Welcome to the APftML2 tool."

:start
echo.
echo 1. The-Frog-King
echo 2. Rumpelstiltskin
echo 3. The-Bremen-Town-Musicians
set /p choice="Please select a fairy tale (1,2,3)"
if '%choice%'=='' echo "%choice%" is not valid please try again
if '%choice%'=='1' (
    set id=1
    set name=The-Frog-King.txt
    goto step2
)
if '%choice%'=='2' (
    set id=2
    set name=Rumpelstiltskin.txt
    goto step2
)
if '%choice%'=='3' (
    set id=4
    set name=The-Bremen-Town-Musicians.txt
    goto step2
)
echo.
goto start

:step2
set /p "answer=enable the TTS module? (y/n)"
if '%answer%'=='y' (
    set tts=1
    goto process
)
if '%answer%'=='n' (
    set tts=0
    goto process
)
echo.
goto start

:process
@rem Step 1: look up references
java -classpath demo.jar exec.OntologyRefLookupExecutor init.owl http://www.sopro13.de/SoPro# ./SoPro13Python/emotion/references.xml %id%

@rem Step 2: extract time, dialogues and emotions
cd SoPro13Python
python Demo.pyc %name% %id%

@rem Step 3: populate the ontology
cd ..
java -classpath demo.jar exec.OntologyPopulatorExecutor init.owl http://www.sopro13.de/SoPro# annotated %id%

if '%tts%'=='1' (
    @rem Step 4: extract the script and play    
    java -classpath demo.jar exec.StoryPlayerExecutor init.owl http://www.sopro13.de/SoPro# %id%
)
