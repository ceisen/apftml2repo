# Example output files #

This folder contains the result of a run-through of the tool in a UNIX
environment on the raw textfile of "The Frog King". The system specs were as 
follows:

Kernel: 3.13.0-24-generic x86_64   
Desktop: Linux Mint 17 Qiana  
CPU: Triple core AMD Athlon II X3 460  
Memory: 5969.6MB  
Graphics: Advanced Micro Devices [AMD/ATI] Bonaire XTX [Radeon R7 260X]  
Audio: Card-1: NVIDIA MCP61 High Definition Audio driver

## output files ##
file | decription 
---|---
annotated_ft1.xml | the extracted information represented as XML file
init.owl | an ontology populated with the extracted information
audio_output.mp3 | the computed audio output


The audio output was recorded to a WAVE file using "Audio Recorder". Leading 
and trailing silence was then removed using "Audacity". Finally, the file was 
converted to MP3 using "LAME" (VBR, V0). Otherwise it remains unedited.

Audio Recorder https://launchpad.net/audio-recorder  
Audacity http://audacity.sourceforge.net/  
LAME http://lame.sourceforge.net/ 
